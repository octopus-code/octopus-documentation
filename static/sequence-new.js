// change a string to create a valid key:

function tagify(name) {
    let re1=/ /g
    let re2=/\./g
    return name.replace(re1,'-').replace(re2,'-')
}


// ----------------------------------------------------------------------------------------
//
// prepare_systems adds some more information to the systems objects and creates a list of
// 'actors' which contains systems and interactions, i.e. all entities which can be owner 
// of an event, and which should be associated with one column in the graph:

function prepare_systems(systems, constants) {

    var actors = Object();

    Object.keys(systems).forEach( function(key) {

        var current = systems[key];
        var tmp = key.split('.'); 

        actors[key] = current;

        current.name = tmp[tmp.length-1]; // discard parent names;

        current.id = 'system_'+tagify(key);
        current.status = 'expand';
        current.minHeight = constants.systemHeight;
        current.isContainer = current['children'].length>0?true:false;

        current.interactions.forEach( function(i) {
            i.owner = current.name; // key;
            i.name = i.owner+':'+i.type+'-'+i.partner
            i.width = constants.interactionWidth;
            i.show = true;
            i.isGhost = (i.type == 'ghost');
            current[i.type+'-'+i.partner] = i;
            i.belongsToContainer = current.isContainer || (systems['root.'+i.partner]['children'].length>0) ;
            actors[i.name] = i;
        });
    });
    return actors;
}

// ----------------------------------------------------------------------------------------

function get_root_event(events, key) {
    var current = events[key];

    if (current.parent.length == 0) {
        return key; 
    }
    else { 
        return get_root_event(events, current.parent[0]); 
    }
}

function prepare_events(events, systems, constants, top_events, index) {

    var counter = 0;
    Object.keys(events).forEach( function(key) {

        index[counter] = key;
        current = events[key];
        current.step = counter;
        current.id = 'step_'+counter++;
        current.name = current.function;
        current.isInteraction = false;
        current.isClockUpdate = false;
        current.belongsToGhost = false;
        current.belongsToContainer = systems[current.system].isContainer;

        current.show = false;
        current.canCollapse = false;

        current.color = "rgba(255,255,0,0.2)"
        current.textcolor = "black"
        current.minHeight = constants.minHeight;

        // we rely on the fact that children of events are always after the parents in the list.

        current.allClocks = [];

        var root_event = events[get_root_event(events, key)];

        switch(current.function){
            case 'dt_operation': 
                current.name = current.operation;
                if(current.operation == "Propagation step - Updating interactions") {
                    current.color = "grey";
                    current.textcolor = "grey";
                } else  if (current.operation == "Propagation step finished" ) {
                    current.color = "green";
                    current.textcolor = "green";
                } else {
                    current.color = "orange";
                    current.textcolor = "orange";
                }
                root_event.allClocks.push([current.system, current.system_clock[1], parseFloat(current.system_clock[1])>parseFloat(current.system_clock[0])])
            break;
            case 'multisystem_dt_operation': 
                current.canCollapse = true;
                if (current.system=='root') top_events.push(key)
            break;
            case 'system_propagation_start':
            case 'system_algorithm_start':
                current.color = "grey"
                root_event.allClocks.push([current.system, current.system_clock[1], parseFloat(current.system_clock[1])>parseFloat(current.system_clock[0])])
            break;
            case 'system_propagation_finish':
            case 'system_algorithm_finish':
                current.color = "grey"
                root_event.allClocks.push([current.system, current.system_clock[1], parseFloat(current.system_clock[1])>parseFloat(current.system_clock[0])])
            break;
            case 'multisystem_propagation_start':
            case 'multisystem_algorithm_start':
                current.color = "rgba(0,255,0,0.2)"
                current.canCollapse = true;
                if (current.system=='root') top_events.push(key)
                root_event.allClocks.push([current.system, current.system_clock[1], parseFloat(current.system_clock[1])>parseFloat(current.system_clock[0])])
            break;
            case 'multisystem_propagation_finish':
            case 'multisystem_algorithm_finish':
                current.color = "rgba(255,0,0,0.2)"
                current.canCollapse = true;
                if (current.system=='root') top_events.push(key)
            break;
            case 'interaction_with_partner_update': 
                current.isInteraction = true; 
                current.color = "blue";
                current.textcolor = "blue";
                if( current.target.split('-')[0] == 'ghost') {
                    current.belongsToGhost = true;
                    current.color = "lightblue";
                    current.textcolor = "blue";
                    }
                // root_event.allClocks.push([current.system+':'+current.target, current.interaction_clock[1]])
                current.partner = 'root.'+current.target.split('-')[1].trim();
                current.belongsToContainer = current.belongsToContainer || systems[current.partner].isContainer; 
            break;
            case 'system_update_exposed_quantities':
                if (step_belongs_to_ghost(events, key, '')) {
                    current.color = "pink"; 
                    current.textcolor = "red";
                } else {
                    current.color = "red"; 
                    current.textcolor = "red";
                }
                var parent = current.parent[0];
                current.belongsToContainer = current.belongsToContainer && events[parent].belongsToContainer; 
            break;
            case 'marker': 
                current.canCollapse = false;
                current.name = current.text;
                current.color = 'black';
                if (current.system=='root') top_events.push(key)
            break;
            case 'clock_update':
                current.canCollapse = false;
                current.isClockUpdate = true;
                current.color = 'yellow';
                if (current.clock_name == 'interaction') {
                    current.partner = 'root.'+current.clock_detail.split('-')[1].trim();   
                    if( current.clock_detail.split('-')[0] == 'ghost') current.belongsToGhost = true;
                }
                current.belongsToContainer = current.belongsToContainer && events[current.parent[0]].belongsToContainer; 
                current.name = current.clock_name+' '+current.clock_detail+' clock update: '+ current.action;
            break;
            }

        current.status = 'expand'; 
        constants.max_steps = counter;

    });

    return top_events;
}


// ----------------------------------------------------------------------------------------

function create_step_groups(events, top_events, top_group) {

    for(step of top_events) {
        var current = events[step];
        var compound_steps = [];

        top_group.append('g').attr('id',tagify(current.id));

        var svg_sub_group = top_group.selectAll('g#'+current.id);

        for (child of current.children) {
            compound_steps.push(child)
        }
        create_step_groups(events, compound_steps, svg_sub_group);

    }
}

// ----------------------------------------------------------------------------------------

function event_get_height(events, key, constants, viewGhosts) {

    var current = events[key];
    var height;
    
    switch(current.status){ 
    case 'expand': 
        height = (viewGhosts || ! current.belongsToGhost)?current.minHeight:0;
        for( child of current.children ) {
            height += event_get_height(events, child, constants, viewGhosts) + constants.stepMargin;
        }
        if (current.parent.length==0) height += constants.stepMargin;
    break;
    case 'collapse':
        height  = constants.minHeight + constants.stepMargin;
    break;
    case 'hide':
        height = 0;
        for( child of current.children ) {
            height += event_get_height(events, child, constants, viewGhosts) + constants.stepMargin;
        }
        if (current.parent.length==0) height += constants.stepMargin;
    break;
    }

    return height;
}

function system_get_width(systems, key) {

    var current = systems[key];
    var width = constants.systemMinWidth;


    if(current.show) {
        for( child of current['children'] ) {
            width += system_get_width(systems, child) + 5;
        }
        for( interaction of current['interactions'] ) {
            width += interaction.width;
        }
    }


    return width;
}

function system_get_y_offset(systems, key) {

    var current = systems[key];
    var offset = 0;

    for (parent of current['parent'])  {
        offset += system_get_y_offset(systems, parent) + systems[parent].height + 5;
    }
    return offset;
}

function all_finished(events, key) {

    var current = events[key];
    var result = false;

    if( current.function == 'multisystem_dt_operation') {
        result = true;
        for( child of current['children']) {
            result = all_finished(events, child) && result;
        }
    }
    else if (current.operation == 'Propagation step finished') {
        result = true;
    }
    return result;
}

function contains_highlight(events, key, highlight) {

    var current = events[key];
    var result = false;

    if(current.step == highlight) {
        result = true;
    } else {
        for (child of current.children) {
            result = result || contains_highlight(events, child, highlight);
        }
    }

    return result;
}

function step_belongs_to_ghost(events, key, prefix) {
    
    var current = events[key];
    var result;

    if( current.belongsToGhost ) {
        result = true;
    } else {
        result = false;
        for(parent of current.parent) {
            result = result || step_belongs_to_ghost(events, parent, prefix+' ');
        }
    }

    return result;
}

function step_is_visible(data, key) {


        if( !Object.keys(data.events).includes(key) ) return false;

        var current = data.events[key];

        var result = data.viewContainers || !data.systems[current.system].isContainer;
        result = result && (data.viewGhosts || !step_belongs_to_ghost(data.events, key,''));

        return result;
}



function run(data, my_global_tag, viewGhosts, viewContainers, initial_step) {

    var systems = data.systems;
    var events = data.events;

    data.constants = Object();
    
    data.constants.minHeight  = 20;
    data.constants.minWidth = 50;
    data.constants.stepHeight = 20; // we still might want to make this more flexible, depending on the content of the steps.
    data.constants.stepMargin = 10;
    data.constants.systemMargin = 10;
    data.constants.systemHeight = 30;
    data.constants.systemMinWidth = 250;
    data.constants.interactionWidth = 50; // this should be determined from the text width

    data.viewContainers = viewContainers;
    data.viewGhosts = viewGhosts;
    data.viewClocks = false;

    data.highlight = initial_step;

    console.log('run with '+my_global_tag)

    var top_div = d3.selectAll("div#"+my_global_tag)

//    top_div.remove();


    var header_div = top_div.append('div').attr('id','header'+my_global_tag).attr('class','headcontainer').style('position','relative')
    var main_div   = top_div.append('div').attr('id','main'+my_global_tag).attr('class','stepcontainer')


    header_div
    .append('svg')
    .attr('id','head'+my_global_tag)
    .attr('width','100%')

    var display = header_div.append('div').attr('class','display')

    main_div    
    .append('svg')
    .attr('id','main'+my_global_tag)
    .attr('width','100%')

    data.top_div = main_div;

    // declare the arrow heads:
    var svg = main_div.select("svg#main"+my_global_tag);
    var svg_head = top_div.select("svg#head"+my_global_tag);

    svg.selectAll('*').remove()
    svg_head.selectAll('*').remove()

    var arrowPoints = [[0, 0], [0, 10], [10, 5]];

    svg
    .append('defs')
    .append('marker')
    .attr('id', 'arrow')
    .attr('viewBox', [0, 0, 10,10])
    .attr('refX', 10)
    .attr('refY', 5)
    .attr('markerWidth', 10)
    .attr('markerHeight', 10)
    .attr('orient', 'auto-start-reverse')
    .append('path')
    .attr('d', d3.line()(arrowPoints))
    .attr('stroke', 'black');


    var control = svg_head.append('g').attr('id','control').attr('transform','translate(30,0)');
    var header = svg_head.append('g').attr('id','header').attr('transform','translate( 50, 0 )');
    var main   = svg.append('g').attr('id','main').attr('transform','translate( 50, 100 )');
    var footer = svg.append('g').attr('id','footer').attr('transform','translate( 50, 0 )');
    
    var tooltip = main_div.append('div').attr('id','tooltip').attr('class','tooltip')


    control.append('rect').attr('class','controlbutton')
    .attr('x',0)
    .attr('y',5)
    .attr('rx',20)
    .attr('ry',20)
    .attr('width',180)
    .attr('height',40)
    .attr('id','ContainerSwitch')
    .on("click", function(event) { toggle_container_view(data); })

    control.append('text').attr('id','ContainerSwitch')
    .text(data.viewContainers?'Hide Containers':'Show Containers')
    .attr('class','sequence-normal')
    .attr('x',15)
    .attr('y',30)
    .on("click", function(event) { toggle_container_view(data); })

    control.append('rect').attr('class','controlbutton')
    .attr('x',0)
    .attr('y',55)
    .attr('rx',20)
    .attr('ry',20)
    .attr('width',180)
    .attr('height',40)
    .attr('id','GhostSwitch')
    .on("click", function(event) { toggle_ghost_view(data); })

    control.append('text').attr('id','GhostSwitch')
    .text(data.viewGhosts?'Hide Ghosts':'Show Ghosts')
    .attr('class','sequence-normal')
    .attr('x',15)
    .attr('y',80)
    .on("click", function(event) { toggle_ghost_view(data); })

    control.append('rect').attr('class','controlbutton')
    .attr('x',0)
    .attr('y',105)
    .attr('rx',20)
    .attr('ry',20)
    .attr('width',180)
    .attr('height',40)
    .attr('id','ExpandAllSwitch')
    .on("click", function(event) { expand_all(data); })

    control.append('text').attr('id','ExpandAllSwitch')
    .text('Expand All')
    .attr('class','sequence-normal')
    .attr('x',15)
    .attr('y',130)
    .on("click", function(event) { expand_all(data); })


    control.append('rect').attr('class','controlbutton')
    .attr('x',0)
    .attr('y',155)
    .attr('rx',20)
    .attr('ry',20)
    .attr('width',180)
    .attr('height',40)
    .attr('id','CollapseAllSwitch')
    .on("click", function(event) { collapse_all(data); })

    control.append('text').attr('id','CollapseAllSwitch')
    .text('Collapse All')
    .attr('class','sequence-normal')
    .attr('x',15)
    .attr('y',180)
    .on("click", function(event) { collapse_all(data); })


    control.append('rect').attr('class','controlbutton')
    .attr('x',0)
    .attr('y',205)
    .attr('rx',20)
    .attr('ry',20)
    .attr('width',180)
    .attr('height',40)
    .attr('id','ToggleClocks')
    .on("click", function(event) { toggle_clocks(data); })

    control.append('text').attr('id','ToggleClocks')
    .text(data.viewClocks?'Hide Clocks':'Show Clocks')
    .attr('class','sequence-normal')
    .attr('x',15)
    .attr('y',230)
    .on("click", function(event) { toggle_clocks(data); })


    control.append('rect').attr('class','controlbutton')
    .attr('x',200)
    .attr('y',5)
    .attr('rx',20)
    .attr('ry',20)
    .attr('width',40)
    .attr('height',40)
    .attr('id','ForwardSwitch')
    .on("click", function(event) { forward(data); })

    control.append('text').attr('id','ForwardSwitch')
    .text('+')
    .attr('class','sequence-normal')
    .attr('x',215)
    .attr('y',30)
    .on("click", function(event) { forward(data); })

    control.append('rect').attr('class','controlbutton')
    .attr('x',200)
    .attr('y',55)
    .attr('rx',20)
    .attr('ry',20)
    .attr('width',40)
    .attr('height',40)
    .attr('id','BackwardSwitch')
    .on("click", function(event) { backward(data); })

    control.append('text').attr('id','BackwardSwitch')
    .text('-')
    .attr('class','sequence-normal')
    .attr('x',215)
    .attr('y',80)
    .on("click", function(event) { backward(data); })


    // prepare global time arrow:

    svg.append('line')
    .attr('x1',10)
    .attr('y1',50)
    .attr('x2',10)
    .attr('y2',50)
    .attr('id','global_time_line')
    .attr('marker-end', 'url(#arrow)')
    .attr('stroke', 'black');
    

    console.log("running main script.");

    data.actors = prepare_systems(systems, data.constants);

    console.log(data.actors);


    var top_events = [];
    
    data.index = [];
    prepare_events(events, systems, data.constants, top_events, data.index);

    create_step_groups(events, top_events, main);

    data.top_events = top_events;     


    // collect svg areas in data.svg:

    data.svg = Object();
    data.svg.all = svg;
    data.svg.head = svg_head;
    data.svg.control = control;
    data.svg.header = header;
    data.svg.main = main;
    data.svg.footer = footer;
    data.tooltip = tooltip;
    data.display = display;

    update(data);

}

function collect_operations(events, start, operations, viewContainers) {

    var current = events[start];

    if( current.function == 'MARKER' ) {
        operations.push( {system:current.system, op:current.name, step:current.step});
    } else if( current.function == 'system_propagation_start' || current.function == 'system_algorithm_start' ) {
        operations.push( {system:current.system, op:'algorithm_start', step:current.step});
    } else if( current.function == 'system_propagation_finish' || current.function == 'system_algorithm_finish' ) {
        operations.push( {system:current.system, op:'algorithm_finish', step:current.step});
    } else if( current.function == 'dt_operation' && (viewContainers || !current.belongsToContainer)) {
        if(/- /.test(current.operation)) {
            operations.push( {system:current.system, op:current.operation.substring(17).replace('- ',''), step:current.step});
        } else {
            operations.push( {system:current.system, op:current.operation, step:current.step});
        } 
    } else {
        for( child of current.children ) collect_operations(events, child, operations, viewContainers);
    }
}

function draw_Interactions(systems, key, constants, header, viewGhosts, viewContainers) {

    system_ = systems[key];
    var tag = tagify(system_.id);

    var filtered_interactions = [];

    if (system_.status == 'expand') {
        for(interaction of system_.interactions) {
            if( (viewGhosts || interaction['type']!='ghost') && (viewContainers || !interaction.belongsToContainer ) ) {
                filtered_interactions.push(interaction)
            }
        }
    }

    header.selectAll('rect#'+tag).remove();
    header.selectAll('text#'+tag+'_1').remove();
    header.selectAll('text#'+tag+'_2').remove();


    var interactions      = header.selectAll('rect#'+tag).data(filtered_interactions);
    var interactions_txt1 = header.selectAll('text#'+tag+'_1').data(filtered_interactions);
    var interactions_txt2 = header.selectAll('text#'+tag+'_2').data(filtered_interactions);


    interactions.enter().append('rect')
    .attr('id',tag)
    .attr('x', s => s.x )
    .attr('y', s => s.y)
    .attr('rx',2)
    .attr('ry',2)
    .attr('width', s=>s.width)
    .attr('height', 40)
    .attr('stroke', 'black')
    .attr('fill', s=>(s.isGhost?'lightBlue':'blue'))

    interactions
    .attr('x', s => s.x )
    .attr('y', s => s.y)
    .attr('width', s=>s.width)


    if(system_.status == 'expand' || system_.status == 'collapse' ) {

        interactions_txt1.enter().append('text')
        .text( function(d) {return d.type})
        .attr('class','sequence-interactions')
        .attr('id',tag+'_1')
        .attr('x', s => s.x + 5)
        .attr('y', s => s.y + 15)
        .attr('stroke', s=>(s.isGhost?'black':'white'))

        interactions_txt1
        .attr('x', s => s.x + 5)
        .attr('y', s => s.y + 15)

        interactions_txt1.exit().remove()

        interactions_txt2.enter().append('text')
        .text( function(d) {tmp = d.partner.split('.'); return tmp[tmp.length-1]})
        .attr('class','sequence-interactions')
        .attr('id',tag+'_2')
        .attr('x', s => s.x + 5)
        .attr('y', s => s.y + 30)
        .attr('stroke', s=>(s.isGhost?'black':'white'))

        interactions_txt2
        .attr('x', s => s.x + 5)
        .attr('y', s => s.y + 30)

        interactions_txt2.exit().remove()

    } else {
        header.selectAll('text#'+tag+'_1').remove()
        header.selectAll('text#'+tag+'_2').remove()
    }
}

function clock_string(current, clock_name) {

    var clock = current[clock_name+'_clock'];
    var display = '';

    if(clock.length>0) {
        if( clock[1] > clock[0] ) {
            display += '<tr><th>'+clock_name+':</th> <th>'+clock[0]+'</th>  <th>'+clock[1]+"</th></tr>"
        } else {
            display += '<tr><th>'+clock_name+':</th> <td>'+clock[0]+'</td>  <td>'+clock[1]+"</td></tr>"
        }
    }
    return display;
}

function tooltip_string(current) {

    var display_string = current.step + ': <b><i>'+current.name+"</i></b><br/>"+"<br/>"; 


    switch(current.type) {
        case 'function_call':
            display_string += "<b>System:</b> "+current.system+"</br>";
            display_string += "<table class='display'>"
            display_string += "<tr><th>Clocks:</th><td>In</td><td>Out</td></tr>";
            display_string += clock_string(current, 'system');
            display_string += clock_string(current, 'prop');
            display_string += clock_string(current, 'interaction');
            display_string += clock_string(current, 'partner');
            display_string += clock_string(current, 'requested');
            if( typeof(current.target)!='undefined' && current.target!="undefined" ) display_string += "<b>target:</b>  "+current.target+"</br>";
            if( typeof(current.updated)!='undefined' && current.updated!="undefined" ) display_string += "<b>updated:</b>  "+current.updated+"</br>";
            display_string += "</table>"
        break;
        case 'clock_update':
            display_string += "<b>System:</b> "+current.system+"</br>";
            display_string += "<b>Clock type</b> "+current.clock_name+" "+current.clock_detail+"</br>";
            display_string += "<b>Clock action</b> "+current.action+"</br>";
            display_string += "<b>new time</b> "+current.clock+"</br>";
        break;
    }
    return display_string;
}

function draw_step(data, key, group, step_y, x_label, prefix) {

    var systems = data.systems;
    var events = data.events;
    var constants = data.constants;
    var viewGhosts = data.viewGhosts;

    var current = events[key];
    var step_id = '#'+current.id;
    var system = systems[current.system];

    var x=current.x;
    var y=0;
    var height = event_get_height(events, key, constants, viewGhosts) ;
    var width=current.width;
    var delta_y = 0; //constants.minHeight;


    var step_group = group.selectAll(step_id)
    step_group.attr('transform','translate(0,'+step_y+')');
    step_group.selectAll('rect').remove()
    step_group.selectAll('text').remove()

    switch(current.status) {

        case 'expand':
            delta_y = constants.minHeight;
            step_y += height + constants.stepMargin;
            step_group.insert('rect','g')
            .attr('id',current.id)
            .attr('x', x)
            .attr('y', y)
            .attr('width', width)
            .attr('height', height)
            .attr('fill',current.color)
            .attr('stroke','black')
            .attr('stroke-width', current.step==data.highlight?6:1)
            .on('click', function(event) { click_step(event, current, data); } )
            .on("mouseover", function(event) {	
                var display_string = tooltip_string(current)
                data.tooltip.transition().duration(200)		
                    .style("opacity",1.0);		
                data.tooltip
                    .html(display_string + "<br/>"  )	
                    .style("left", (d3.pointer(event, data.svg.main)[0] + 10) + "px")		
                    .style("top",  (d3.pointer(event, data.svg.main)[1] - 28) + "px");	
                })	
            .on("mousemove", function(event) {
                data.tooltip
                .style("left", (d3.pointer(event, data.svg.main)[0] + 10) + "px")		
                .style("top",  (d3.pointer(event, data.svg.main)[1] - 28) + "px");	
            })				
            .on("mouseout", function(event) {		
                data.tooltip.transition().duration(200)
                    .style("opacity", 0);	
            })

            step_group.insert('text','g')
            .text(current.name)
            .attr('class','sequence-normal')
            .attr('id',current.id)
            .attr('x', x_label)
            .attr('y', 15)
            .attr('fill',current.textcolor)
            .attr('font-weight',current.step==data.highlight?'bold':'normal')

            if(current.isClockUpdate) {
                var clock_symbol;

                switch(current.action) {
                    case 'tick':    clock_symbol='+' ; break;
                    case 'reverse': clock_symbol='-' ; break;
                    case 'set':     clock_symbol='=' ; break;
                    case 'reset':   clock_symbol='.' ; break;
                }

                step_group.insert('text','g')
                .text(clock_symbol)
                .attr('class','sequence-normal')
                .attr('id','current.id')
                .attr('x',x+10)
                .attr('y',15)
                .attr('fill',current.textcolor)
                .attr('font-weight',current.step==data.highlight?'bold':'normal')
                .on("mouseover", function(event) {	
                    var display_string = tooltip_string(current)
                    data.tooltip.transition().duration(200)		
                        .style("opacity",1.0);		
                    data.tooltip
                        .html(display_string + "<br/>"  )	
                        .style("left", (d3.pointer(event, data.svg.main)[0] + 10) + "px")		
                        .style("top",  (d3.pointer(event, data.svg.main)[1] - 28) + "px");	
                    })	
                .on("mousemove", function(event) {
                    data.tooltip
                    .style("left", (d3.pointer(event, data.svg.main)[0] + 10) + "px")		
                    .style("top",  (d3.pointer(event, data.svg.main)[1] - 28) + "px");	
                })				
                .on("mouseout", function(event) {		
                    data.tooltip.transition().duration(200)
                        .style("opacity", 0);	
                })
                    }

            current.children.forEach( function(child) { delta_y = draw_step(data, child, group, delta_y, x_label, prefix+'  ' )} )

            if(current.parent.length>0 && 
                events[current.parent].function!='multisystem_dt_operation' &&
                (events[current.parent].function!='multisystem_propagation_start'  || events[current.parent].function!='multisystem_algorithm_start' ) &&
                (events[current.parent].function!='multisystem_propagation_finish' || events[current.parent].function!='multisystem_algorithm_finish') ) {
                var par_x = events[current.parent].x 
                var stroke_dash = current.updated=="true"?"5,0":"5,5"
                var stroke_width  = "1px"

                if(par_x > x) {

                    step_group.append('rect','g')
                    .attr('id',current.id)
                    .attr('x', x + current.width )
                    .attr('y', y)
                    .attr('width', par_x - x - events[current.parent].width)
                    .attr('height', height )
                    .attr('fill','none')
                    .attr('stroke','black')
                    .attr('stroke-width',stroke_width)
                    .attr('stroke-dasharray',stroke_dash)
                    
                } else {

                    step_group.append('rect','g')
                    .attr('id',current.id)
                    .attr('x', par_x + events[current.parent].width )
                    .attr('y', y)
                    .attr('width', x - par_x - current.width)
                    .attr('height', height)
                    .attr('fill','none')
                    .attr('stroke','black')
                    .attr('stroke-width',stroke_width)

                }
    
            }

        break;

        case 'collapse':
            step_y += constants.minHeight+10;
            step_group.insert('rect','g')
            .attr('id',current.id)
            .attr('x', x)
            .attr('y', 0)
            .attr('width', width)
            .attr('height', constants.minHeight+10)
            .attr('fill',current.color)
            .attr('stroke','black')
            .attr('stroke-width', current.step==data.highlight?4:1)
            .on('click', function(event) { click_step(event, current, data); } )
            .on("mouseover", function(event) {	
                var display_string = tooltip_string(current); 
                data.tooltip.transition().duration(200)		
                    .style("opacity",1.0);		
                data.tooltip
                    .html(display_string + "<br/>"  )	
                    .style("left", (d3.pointer(event, data.svg.main)[0] + 10) + "px")		
                    .style("top",  (d3.pointer(event, data.svg.main)[1] - 28) + "px");	
                    })	
            .on("mousemove", function(event) {
                data.tooltip
                .style("left", (d3.pointer(event, data.svg.main)[0] + 10) + "px")		
                .style("top",  (d3.pointer(event, data.svg.main)[1] - 28) + "px");	
            })				
            .on("mouseout", function(event) {		
                data.tooltip.transition().duration(200)
                    .style("opacity", 0);	
            })

            ops = [];
            collect_operations(events, key, ops, data.viewContainers);
            for(operation of ops) {
                step_group.insert('text','g')
                .attr('id',current.id)
                .attr('class','sequence-normal')
                .text(operation.op)
                .attr('x', systems[operation.system].x)
                .attr('y', y+20)
                .attr('font-weight',operation.step==data.highlight?'bold':'normal')
                .attr('fill',operation.textcolor)
                    
            }
        break;

        case 'hide':
            step_y += height;
            current.children.forEach( function(child) { delta_y = draw_step(data, child, group, delta_y, x_label, prefix+'  ' )} )
        break;

    }

    return step_y;

}

function process_system_geometry(systems, key, offset_x, offset_y, constants, viewContainers, viewGhosts) {

    var current = systems[key]

    var result = Object();
    var delta_y = 0;
    var max_x = 0;
    var max_y = 0;
    var new_max_y = 0;

    current.x = offset_x;
    current.y = offset_y;
    current.height = constants.systemHeight;

    var offset = offset_x 
    
    if( viewContainers || ! current.isContainer  ) {
        offset += constants.interactionWidth + 5;
        for (interaction of current.interactions) {
            if ( (!interaction.isGhost || viewGhosts ) && (!interaction.belongsToContainer || viewContainers ) ) {
                interaction.x = offset;
                offset += interaction.width+5;
            }
        }   
        delta_y = current.height+constants.stepMargin;
        offset = Math.max(offset, offset_x+constants.systemMinWidth);
    } else {
        delta_y = 0;
    }

    for (child of current.children) {
        tmp_result = process_system_geometry(systems, child, offset, offset_y+delta_y, constants, viewContainers, viewGhosts);
        offset += tmp_result.width + 10;
        max_x = Math.max(offset, tmp_result.max_x);
        new_max_y = Math.max(max_y, tmp_result.max_y);
    }

    if( !viewContainers && current.isContainer ) {

        result.width = offset - offset_x;
        result.max_x = max_x;
        result.max_y = new_max_y;
        current.x = offset_x;
        current.y = offset_y;
        current.height = constants.systemHeight;
        current.width = offset - offset_x;

    } else {

        current.width = offset - offset_x;
        result.width  = offset - offset_x;
        result.max_x = max_x;
        result.max_y = new_max_y + current.height + constants.stepMargin;

    }

    return result;
}


function update(data) {

    // update offsets:

    var events     = data.events;
    var systems    = data.systems;
    var constants  = data.constants;
    var top_events = data.top_events;

    var svg    = data.svg.all;
    var svg_head = data.svg.head;
    var header = data.svg.header;
    var main   = data.svg.main;
    var footer = data.svg.footer;

    var x = 0;
    var y = 0;

    // start with 'root':

    var geom = process_system_geometry(systems, 'root', 0, 5, constants, data.viewContainers, data.viewGhosts);

    var max_y = geom.max_y + 20;
    var max_x = geom.max_x + 100;

    var active_systems = [];
    var active_events = [];

    Object.keys(systems).forEach( function(key) {
        d = systems[key];
        d.interactions.forEach( function(i) {
            i.y = max_y;
        });

        if ( data.viewContainers || !d.isContainer ) { 
            active_systems.push(d);
            d.status = 'expand'; 
        } else {
            d.status = 'hide';
        }
        if (d.show == false) d.status='hide'
    });

    main.attr('transform','translate(50,0)')

    y = max_y + 20;

    Object.keys(events).forEach( function(d) {  

        var current = events[d];
        var system  = systems[current['system']];
        var isMulti = current.function=="multisystem_dt_operation" || 
                      current.function=="multisystem_propagation_start" || current.function=="multisystem_algorithm_start" ||
                      current.function=="multisystem_propagation_finish" || current.function=="multisystem_algorithm_finish" ||
                      current.function=="marker";

        current.status = 'expand';

        if (current.belongsToGhost && !data.viewGhosts) current.status = 'hide';
        if (systems[current.system].isContainer && !data.viewContainers &&  current.function!=="marker") current.status = 'hide';

        if (current.function == "system_update_exposed_quantities") {
            var parent = events[current.parent[0]];
            if( parent.belongsToGhost && !data.viewGhosts) current.status = 'hide';
            if( (current.belongsToContainer || parent.belongsToContainer) && !data.viewContainers) current.status = 'hide';
        }
        
        if ( current.parent.length==0 && isMulti) y+=20;

        current.y = y;  
        current.width = isMulti?system.width+10:constants.minWidth;
        current.height = event_get_height(events, d, constants, data.viewGhosts);

        if (current.isInteraction) {
            current.x = system[current.target].x;
        } else if ( current.isClockUpdate ) {
            switch (current.clock_name) {
                case 'interaction':
                    current.x = data.actors[system.name+':'+current.clock_detail].x;
                    break;
                default: 
                    current.x = system.x;
            }
            current.x += 5;
            current.width -= 10;
        } else {
            current.x = system.x;
        }

        if ( isMulti ) {
            current.x -= 5;
            current.height += 5;
        }
        
        if( (data.viewContainers || !system.isContainer) && (data.viewGhosts || !current.belongsToGhost) ) {
            active_events.push(current);
            y+=constants.minHeight+5;
        } 
        if (current.show == false && current.canCollapse && !contains_highlight(events, d, data.highlight) ) {
            if (current.status!='hide' || isMulti) current.status='collapse'
        }
    });

    var sysline1 = header.selectAll('rect#system').data(active_systems);
    var sysline1_txt = header.selectAll('text#system').data(active_systems);

    sysline1.enter().append('rect')
        .attr('id','system')
        .attr('x', d => d.x )
        .attr('y', d => d.y )
        .attr('rx',5)
        .attr('ry',5)
        .attr('width', d=>d.width)
        .attr('height', d => d.height)
        .attr('stroke', 'black')
        .attr('fill', 'none')

    sysline1
        .attr('x', d => d.x )
        .attr('y', d => d.y )
        .attr('width', d=>d.width)

    sysline1.exit().remove()

    sysline1_txt.enter().append('text')
        .text( d => d.name )
        .attr('id','system')
        .attr('x', d => 20 + d.x )
        .attr('y', d => 20 + d.y )
        .attr('stroke', 'black')
        .attr('visibility', d => (d.status!='hide'?'visible':'hidden'))

    sysline1_txt
    .text( d => d.name )
    .attr('x', d => 20 + d.x )
    .attr('y', d => 20 + d.y )
    .attr('visibility', d => (d.status!='hide'?'visible':'hidden'))

    sysline1_txt.exit().remove()

    Object.keys(systems).forEach( function(key) {draw_Interactions(systems, key, constants, header, data.viewGhosts, data.viewContainers)} )

    // var step_groups = main.selectAll('g.step').data(top_events);

    main.selectAll('line#finished').remove()
    main.selectAll('text#clock').remove()

    var step_y = 0
    top_events.forEach( function(key) { 
        step_y = 10 + draw_step(data, key, main, step_y, max_x, '' ); 
        // showClocks()

        if(data.viewClocks) {
            for ( clock of data.events[key].allClocks ) {
                var item = data.actors[clock[0]];
                if(!item.isContainer || data.viewContainers) {
                    main.append('text')
                    .text(clock[1])
                    .attr('id','clock')
                    .attr('x',item.x)
                    .attr('y',step_y+12)
                    .attr('font-weight', clock[2]?'bold':'normal')
                }
            }
            step_y +=30
        }

        if (all_finished(events, key )) {
            main.append('line')
            .attr('id','finished')
            .attr('x1',-10)
            .attr('y1',step_y+5)
            .attr('x2',max_x+400)
            .attr('y2',step_y+5)
            .attr('stroke','black')
            .attr('stroke-width',2)
            step_y += 20;
        }
    });

    data.display
    .html(tooltip_string(events[data.index[data.highlight]]) + "<br/>"  )

    data.svg.control.attr('transform','translate('+max_x+',5)');
    svg.select('#global_time_line')
       .attr('y2',180+step_y)

    svg.attr('viewBox',     '0 0 '+(Math.max(1200,max_x+500))+' '+ (Math.max(step_y+200, 400))+'')
    svg_head.attr('viewBox','0 0 '+(Math.max(1200,max_x+500))+' '+ (Math.max(270,max_y+60)) )
}


function click_step(event, d, data){
//    console.log('click Step', d.id, d.name, event);
    d.show = !d.show;
    event.stopPropagation();
    update(data)
}



function toggle_container_view(data) {

    data.viewContainers = !data.viewContainers; // move that into data.systems?
    data.svg.control.selectAll('text#ContainerSwitch').text(data.viewContainers?'Hide Containers':'Show Containers')
    update(data);

}

function toggle_ghost_view(data) {

    data.viewGhosts = !data.viewGhosts;
    data.svg.control.selectAll('text#GhostSwitch').text(data.viewGhosts?'Hide Ghosts':'Show Ghosts')
    update(data);

}

function toggle_clocks(data) {

    data.viewClocks = !data.viewClocks;
    data.svg.control.selectAll('text#ToggleClocks').text(data.viewClocks?'Hide Clocks':'Show Clocks')
    update(data);

}

function expand_all(data) {
    Object.keys(data.events).forEach( function(key) {
        data.events[key].show = true;
    })
    update(data);
}

function collapse_all(data) {
    Object.keys(data.events).forEach( function(key) {
        data.events[key].show = false;
    })
    update(data);
}

function forward(data) {
    do {
        data.highlight += 1;
        if (data.highlight > data.constants.max_steps) data.highlight = 0;
    } while(!step_is_visible(data, data.index[data.highlight])) 
    update(data) 
}

function backward(data) {
    do {
        data.highlight -= 1;
        if(data.highlight < 0) data.highlight = data.constants.max_steps;
    }
    while(!step_is_visible(data, data.index[data.highlight])) 
    update(data) 
}

function make_ID(id) {
    return 'step_'+id.toString().padStart(8,'0');
}

function contains(list, object) {
    return list.some(element => {
        return JSON.stringify(element)===JSON.stringify(object);
    });
}

function parse_octopus_output(source) {

    data = Object();

    data.systems = Object();
    data.events = Object();

    var ID_stack = [];
    var system_stack = [''];

    for (line of source.split('\n')) {

        var new_entry = Object();

        const words = line.split('|');
        const first = words[0].split(':')
        const id = parseInt(first[1]);
        const key_ID = make_ID(id);
    
        if (first[0].search('IN')>=0) {

            new_entry = Object();
            new_entry['in_ID']=id;
            new_entry['children']=[];
            new_entry['parent']=[];
            new_entry['system_clock']=[];
            new_entry['prop_clock']=[];
            new_entry['algo_clock']=[];
            new_entry['interaction_clock']=[];
            new_entry['partner_clock']=[];
            new_entry['requested_clock']=[];

            if(words.length>0) {
                for ( pair of words.slice(1, words.length) ) {
                    const tmp =  pair.split(':');
                    key = tmp[0];
                    val = tmp[1];
                    if      (key.trim() == 'system_clock')      new_entry['system_clock'].push(val.trim());
                    else if (key.trim() == 'prop_clock')        new_entry['prop_clock'].push(val.trim());
                    else if (key.trim() == 'algo_clock')        new_entry['algo_clock'].push(val.trim());
                    else if (key.trim() == 'interaction_clock') new_entry['interaction_clock'].push(val.trim());
                    else if (key.trim() == 'partner_clock')     new_entry['partner_clock'].push(val.trim());
                    else if (key.trim() == 'requested_clock')   new_entry['requested_clock'].push(val.trim());
                    else                                        new_entry[key.trim()] = val.trim();
                };
            }

            if (ID_stack.length > 0) {
                var tmp_id = ID_stack[ID_stack.length-1];
                new_entry['parent'].push(make_ID(tmp_id));
                data.events[make_ID(tmp_id)]['children'].push(make_ID(id));
            };

            if (new_entry['system'] == 'KEEP') new_entry['system'] = system_stack[system_stack.length-1];

            system_stack.push(new_entry['system']);

            new_entry['system'] = 'root'+new_entry['system'];

            system_name = new_entry['system'];

            if (! (system_name in data.systems) ) {
                data.systems[system_name] = Object();
                data.systems[system_name]['interactions'] = [];
                data.systems[system_name]['children'] = [];
                data.systems[system_name]['parent'] = [];
            };

            if ( 'target' in new_entry ) {
                const tmp = new_entry['target'].split('-');
                const interaction = {'type': tmp[0], 'partner': tmp[1]};
                const current = data.systems[new_entry['system']];

                if ( !contains(current['interactions'],interaction)  ) {
                    current['interactions'].push(interaction);
                }
            };

            data.events[key_ID] = new_entry;
            ID_stack.push(id);
        };

        if (first[0].search('OUT')>=0) {
            ID_stack.pop()
            system_stack.pop()
    
            for (pair of words.slice(1,words.length) ) {
                tmp = pair.split(':');
                key = tmp[0];
                val = tmp[1];
                if (key.search('closes')>=0 ) {
                    enter_ID = make_ID( parseInt(val) )
                    data.events[enter_ID]['out_ID'] = parseInt(id)
                }
                if (key.search('update')>=0 )           data.events[enter_ID]['updated'] = val.trim()
                if (key.trim() == 'system_clock')       data.events[enter_ID]['system_clock'].push(val.trim())
                if (key.trim() == 'prop_clock')         data.events[enter_ID]['prop_clock'].push(val.trim())
                if (key.trim() == 'algo_clock')         data.events[enter_ID]['algo_clock'].push(val.trim())
                if (key.trim() == 'interaction_clock')  data.events[enter_ID]['interaction_clock'].push(val.trim())
                if (key.trim() == 'partner_clock')      data.events[enter_ID]['partner_clock'].push(val.trim())
                if (key.trim() == 'requested_clock')    data.events[enter_ID]['requested_clock'].push(val.trim())
            }
        }

        if (first[0].search('MARKER')>=0) {

            new_entry = Object();
            new_entry['in_ID']=id;
            new_entry['children']=[];
            new_entry['parent']=[];
            new_entry['system_clock']=[];
            new_entry['prop_clock']=[];
            new_entry['algo_clock']=[];
            new_entry['interaction_clock']=[];
            new_entry['partner_clock']=[];
            new_entry['requested_clock']=[];

            if(words.length>0) {
                for ( pair of words.slice(1, words.length) ) {
                    const tmp =  pair.split(':');
                    key = tmp[0];
                    val = tmp[1];
                    if      (key.trim() == 'type')              new_entry['function'] = val.trim(); 
                    else if (key.trim() == 'system_clock')      new_entry['system_clock'].push(val.trim());
                    else if (key.trim() == 'prop_clock')        new_entry['prop_clock'].push(val.trim());
                    else if (key.trim() == 'algo_clock')        new_entry['algo_clock'].push(val.trim());
                    else if (key.trim() == 'interaction_clock') new_entry['interaction_clock'].push(val.trim());
                    else if (key.trim() == 'partner_clock')     new_entry['partner_clock'].push(val.trim());
                    else if (key.trim() == 'requested_clock')   new_entry['requested_clock'].push(val.trim());
                    else                                        new_entry[key.trim()] = val.trim();
                };
            }

            if (ID_stack.length > 0) {
                var tmp_id = ID_stack[ID_stack.length-1];
                new_entry['parent'].push(make_ID(tmp_id));
                data.events[make_ID(tmp_id)]['children'].push(make_ID(id));
            };

            if (new_entry['system'] == 'KEEP') new_entry['system'] = system_stack[system_stack.length-1];
            new_entry['system'] = 'root'+new_entry['system'];

            data.events[key_ID] = new_entry;
        }

    }

    for (system of Object.keys(data.systems)) {
        names = system.split('.');
        if (names.length == 2) data.systems['root']['children'].push(system);
        if (names.length >  2) data.systems[names.slice(0,names.length-1).join('.')]['children'].push(system);
        if (names.length >  1) data.systems[system]['parent'].push(names.slice(0,names.length-1).join('.'));
    }
    return data;
}


function entry_txt(filename, global_tag, viewGhosts, viewContainers, initial_step) {
    var data = parse_octopus_output(filename);

    console.log(data.systems);
    run(data, global_tag, viewGhosts, viewContainers, initial_step);
}



function entry(filename, global_tag, viewGhosts, viewContainers, initial_step) {
    d3.json(filename).then( function(d) {run(d, global_tag, viewGhosts, viewContainers, initial_step)} );
}


function set_display(data, key) {
    data.dislplay = key;
    update(data);
}

// entry(filename, global_tag, viewGhosts, viewContainers, initial_step)

