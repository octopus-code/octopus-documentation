---
Title: "Building the documentation"
Weight: 5
description: ""
---


If you are writing new material for the tutorials or the manual, it is helpful to build the documentation pages locally, before submitting the material to the {{< octopus >}} web server. All that is needed to build the documentation are {{< hugo >}} and a web server, such as [apache](http://httpd.apache.org/), or python's [http.server](https://docs.python.org/3/library/http.server.html) module.

If you already have installed {{< hugo >}} and started the web server, you can continue with {{< versioned-link "developers/building_docs/#building-the-pages" "building the pages" >}}. Otherwise, follow the instructions to install {{< hugo >}} and set up a web server.

## Installing {{< hugo >}}

The documentation system requires {{< hugo >}} at least at version 0.79. {{< hugo >}} can be downloaded [here](https://github.com/gohugoio/hugo). For most operating systems, a binary version can be found [here](https://github.com/gohugoio/hugo/releases).

Once the package is installed, you can check the correct version by
```bash
hugo version
```



## Installing a web server

The easiest option is to use the python's [http.server](https://docs.python.org/3/library/http.server.html) module, this comes installed by default if you have `python3`  installed.

## Cloning the repository

The documentation framework is kept in a git repository at [MPSD](https://mpsd-gitlab2.desy.de). 
This repository contains all necessary files (such as the shortcodes, layouts, etc.) and contains the doc dock theme as a submodule.
Also, the octopus code repository is a submodule of the repository. This is required as the source files as well as testfiles will be parsed while building the documentation, and also since from {{< octopus >}} version 11, all documentation source (i.e. markdown) files will be part of the {{< octopus >}} source repository.

```bash
cd $HUGO_DOC
git clone <repo-name>

cd Octopus
git submodule update --init --recursive
```



## Building the pages
There are two make targets created for the generation of the documentation pages. The first one is `make pages` which will build the pages locally. The second one is to `make docker` which will build the pages via docker. The docker target is the recommended way to build the pages, as it will ensure that the correct version of {{< hugo >}}, ImageMagic and python is used.

### Building the pages locally
You need hugo, ImageMagick and python installed correctly to build the docs locally.
We use the `convert` command from ImageMagick to convert images from postscript to png format.
ImageMagick blocks reading and writing to `.ps` files by default for security reasons ([see](https://imagemagick.org/script/security-policy.php)). You can customize the policy to allow read writes on ps files by editing the `policy.xml` file located in the ImageMagick installation directory. You need to change the line that says:
`<policy domain=“coder” rights=“none” pattern=“PS” />` to `<policy domain=“coder” rights=“read|write” pattern=“PS” />` ([see](https://stackoverflow.com/questions/52703123/override-default-imagemagick-policy-xml)).
To build the docs using {{< octopus >}} source folder present within the current directory and to serve them on port 8080, run:
```bash
make pages 
```
This target can also take optional arguments SRC_OCT and NAME.
- `SRC_OCT` is the path to the octopus source (if not passed octopus folder is assumed at the current working dir).
- `NAME` is the display name for the web pages (default: custom)
Example:
```bash
make SRC_OCT=full_path_to_octopus_repo NAME="main" pages   # build locally with main as name
```
### Building the pages via docker
To build the docs via docker (needs docker correctly installed, docker command runnable without sudo, [see](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)) from {{< octopus >}} source folder present within the current directory and to serve them on port 8080, run:
```bash
make docker 
```
This target can also take optional arguments SRC_OCT and NAME just like the `pages` target.
Example:
```bash
make SRC_OCT=full_path_to_octopus_repo NAME="main" docker   # build via docker with mainas name
```

Building the web pages is done by the script {{< code "build-pages.sh" >}}, which performs the following steps for each selected code version (branch):

* Check out the latest version of each specified {{< octopus >}} branch
* If present, copy {{< basedir >}}/doc/html/content/* to {{< file "$HUGO_DOC/Octopus/content/" >}}
* Parse all {{< octopus >}} source files and build the {{< file "variables.json" >}} file, containing all variable information.
* Parse testsuite input files, as well as tutorial and manual files to create links to variable descriptions.
* Create the variable reference markdown files from the above information.
* Run hugo to create the static pages.

The {{< octopus >}} versions for which the documentation is built are specified in the file {{< file "branches" >}}, which should look like:
```text
# branch name
#
9.2 9
10.3 10
develop develop
```
where the first entry in each line is the name of the {{< name tag >}} of an {{< octopus >}} version, while the second entry is the name under which it should appear on the web pages.
{{< notice note >}}
The order of the lines is important, as the first corresponds to the left-most entry in the Version selector in the header of the web pages. Line can be commented out with a hash, but there must not be any empty lines in the file.
{{< /notice >}}

{{< notice warning >}}
Be aware that the {{< file build-pages.sh >}} script, in general, will checkout the latest version of the {{< octopus >}} repository from git. If you have local changes, which are not committed, they might be lost.
{{< /notice >}}

{{% notice tip %}}
If you are developing tutorial pages and do not want to build all pages, you can leave use the {{< file rebuild-last.sh >}} script.
This will not touch the {{< octopus >}} code repository, but _only_ generate the web pages for the last entry of the {{< file branches >}} file.
It is important, thought, that the full script {{< file build-pages.sh >}} has been run at least once before {{< file rebuild-last.sh >}} can be used.
{{% /notice %}}


# TODO DOCUMENT make SRC_OCT=/full/path/to/octopus ARGS="-n main" pages   # build only main locally
# TODO DOCUMENT make SRC_OCT=/full/path/to/octopus ARGS="-n main" docker   # build only main via docker
