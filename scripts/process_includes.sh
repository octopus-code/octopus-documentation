#!/usr/bin/env bash

OCTOPUS_TOP=$PWD/octopus
OCTOPUS_DOC=$OCTOPUS_DOCs/doc/html/

# Include input files:

grep -r "^#include_input " content/ | sed '{s|:| |g}' > input_lines
grep -r "^#include_input " content/ | sed '{s|{{% ||g}' | cut -d ' ' -f 2 | sed '{s|\"||g}' > include_files
rm -rf static/testsuite

while IFS=' ' read -r testfile; do
    dir=`dirname $testfile`
    outfile=`basename $testfile`
    mkdir -p $PWD/static/$dir
    echo   "process testfiles "$testfile, $dir, $outfile
    $PWD/scripts/annotate_input.py -d $PWD/data/varinfo.json -i $OCTOPUS_TOP/$testfile -o $PWD/static/$dir/$outfile
done < include_files

while IFS=' ' read -r mdfile marker testfile; do
    dir=`dirname $testfile`
    outfile=$dir/`basename $testfile`
    echo   "process testfiles "$mdfile": "$marker" "$outfile
    sed -i -e "\|^$marker $testfile *$|r $PWD/static/$outfile" $mdfile
    sed -i -e "\|^$marker $testfile *$|d" $mdfile
done < input_lines

rm include_files input_lines 


# Include files:

grep -r "^#include_file " content/ | sed '{s|:| |g}' > general_input_lines
grep -r "^#include_file " content/ | sed '{s|{{% ||g}' | cut -d ' ' -f 2 | sed '{s|\"||g}' > include_general_files
rm -rf static/testsuite

while IFS=' ' read -r testfile; do
    dir=`dirname $testfile`
    outfile=`basename $testfile`
    mkdir -p $PWD/static/$dir
    echo   "process include files "$testfile, $dir, $outfile
    cp $OCTOPUS_TOP/$testfile $PWD/static/$dir/$outfile
done < include_general_files

while IFS=' ' read -r mdfile marker testfile; do
    dir=`dirname $testfile`
    outfile=$dir/`basename $testfile`
    echo   "process include files "$mdfile": "$marker" "$outfile
    sed -i -e "\|^$marker $testfile *$|r $OCTOPUS_TOP/$testfile" $mdfile
    sed -i -e "{s|^$marker $testfile *$||g}" $mdfile
done < general_input_lines

#rm include_files input_lines 


# Include tutorial images

grep -r "^#include_eps " content/ | sed '{s|md:|md |g}' > input_eps_lines
grep -r "^#include_eps " content/ | sed '{s|{{% ||g}' | cut -d ' ' -f 2 | sed '{s|\"||g}' > include_eps_files

while IFS=' ' read -r epsfile; do
    dir=`dirname $epsfile`
    outfile=`basename $epsfile eps`png
    mkdir -p $PWD/static/$dir
    echo   "process eps files "$epsfile, $dir, $outfile
    convert -rotate 90 -flatten -density 300 -size 3300x2550 $OCTOPUS_TOP/$epsfile $PWD/static/$dir/$outfile
done < include_eps_files

while IFS=' ' read -r mdfile marker epsfile caption; do
    dir=`dirname $epsfile`
    outfile=/$dir/`basename $epsfile eps`png
    if [[ ! $caption == *"width"* ]]; then
        new_caption=$caption" width=\"500px\""
    else
        new_caption=$caption
    fi
    echo   "process eps files "$mdfile": "$marker" "$outfile" |"$new_caption"|"
    sed -i -e "\|^$marker $epsfile $caption *$|c {{<figure src=\"$outfile\" $new_caption >}}" $mdfile
done < input_eps_lines

# Include type definitions:

grep -r "^#include_type_def" content/ | sed '{s|:| |g}' > def_input_lines
grep -r "^#include_type_def" content/ | cut -d ' ' -f 2 | sed '{s|\"||g}' > type_names

while IFS=' ' read -r typename; do
    sed -n "/^ *type.* $typename *$/,/end type/p" $OCTOPUS_TOP/src/*/*.F90 $OCTOPUS_TOP/src/*/*/*.F90 > $PWD/static/$typename.txt
done < type_names

while IFS=' ' read -r mdfile marker typename; do
    echo   "process def "$mdfile": "$marker" "$typename
    sed -i -e "\|^$marker $typename *$|r $PWD/static/$typename.txt" $mdfile
    sed -i -e "{/^$marker $typename *$/d}" $mdfile
done < def_input_lines

rm type_names def_input_lines 

# Include functions:

grep -r "^#include_function" content/ | sed '{s|:| |g}' > function_input_lines
grep -r "^#include_function" content/ | cut -d ' ' -f 2 | sed '{s|\"||g}' > function_names

while IFS=' ' read -r functionname; do
    sed -n "/function *$functionname(/,/end function/p" $OCTOPUS_TOP/src/*/*.F90 $OCTOPUS_TOP/src/*/*/*.F90 > $PWD/static/$functionname.txt
done < function_names

while IFS=' ' read -r mdfile marker functionname; do
    echo   "process function "$mdfile": "$marker" "$functionname
    sed -i -e "\|^$marker $functionname *$|r $PWD/static/$functionname.txt" $mdfile
    sed -i -e "{/^$marker $functionname *$/d}" $mdfile
done < function_input_lines

rm function_names function_input_lines 

# Include subroutines:

grep -r "^#include_subroutine" content/ | sed '{s|:| |g}' > subroutine_input_lines
grep -r "^#include_subroutine" content/ | cut -d ' ' -f 2 | sed '{s|\"||g}' > subroutine_names

while IFS=' ' read -r subroutinename; do
    sed -n "/subroutine *$subroutinename(/,/end subroutine/p" $OCTOPUS_TOP/src/*/*.F90 $OCTOPUS_TOP/src/*/*/*.F90 > $PWD/static/$subroutinename.txt
done < subroutine_names

while IFS=' ' read -r mdfile marker subroutinename; do
    echo   "process sub "$mdfile": "$marker" "$subroutinename
    sed -i -e "\|^$marker $subroutinename *$|r $PWD/static/$subroutinename.txt" $mdfile
    sed -i -e "{/^$marker $subroutinename *$/d}" $mdfile
done < subroutine_input_lines

rm subroutine_names subroutine_input_lines 

# Include code marked snippets

grep -r "^#include_code_doc" content/ | sed '{s|:| |g}' > doc_input_lines
grep -r "^#include_code_doc" content/ | cut -d ' ' -f 2 | sed '{s|\"||g}' > doc_names

while IFS=' ' read -r docname; do
    sed -n "/^ *\!\# *doc_start *$docname *$/,/^ *\!\# *doc_end *$/p" $OCTOPUS_TOP/src/*/*.F90 $OCTOPUS_TOP/src/*/*/*.F90 | grep -v "doc_start" | grep -v "doc_end" > $PWD/static/doc_$docname.txt
done < doc_names

while IFS=' ' read -r mdfile marker docname; do
    echo   "process code snippet "$mdfile": "$marker" "$docname
    sed -i -e "\|^$marker $docname|r $PWD/static/doc_$docname.txt" $mdfile
    sed -i -e "{s|^$marker $docname||g}" $mdfile
done < doc_input_lines

rm doc_names doc_input_lines

grep -r "^#include_input_snippet " content/ | sed '{s|:| |g}' > input_snippet_lines

while IFS=' ' read -r mdfile marker input_file snippet_name; do
    echo   "process input snippet "$mdfile": "$marker" "$input_file" "$snippet_name

    tmp_file=$PWD/static/`basename $input_file`_$snippet_name.txt
    echo $tmp_name" "$input_file" "$snippet_name
    sed -n "/^ *\# *snippet_start *$snippet_name *$/,/^ *\# *snippet_end *$/p" $OCTOPUS_TOP/$input_file | grep -v "snippet_start" | grep -v "snippet_end" | $PWD/scripts/annotate_input.py -d $PWD/data/varinfo.json > $tmp_file

    sed -i -e "\|^$marker $input_file $snippet_name|r $tmp_file" $mdfile
    sed -i -e "\|^$marker $input_file $snippet_name|d" $mdfile

    rm $tmp_file
done < input_snippet_lines

rm input_snippet_lines 
