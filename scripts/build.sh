#!/usr/bin/env bash


branchname=`echo $OCTOPUS_COMMIT_REF | cut -d"." -f 1`
name=`echo $branchname | sed '{s:/:_:g}'`

sed "{s|OCTOPUSVERSION|$branchname|g;s|OCTOPUSBRANCH|$OCTOPUS_COMMIT_REF|g;s|SITE_PREFIX|$PREFIX|g}" config.toml_TEMPLATE > config.toml
cat config_toml.end >> config.toml

echo building branch $OCTOPUS_COMMIT_REF

rm -rf public/$name
rm -rf public/$PREFIX/$name

hugo --destination "public/$PREFIX/$name" > $name.log 2>&1

rm -rf static/images static/testsuite static/doc static/octopus static/includes
rm -rf static/*.txt
rm -rf static/graph_data
rm -r content/*


# rm config.toml

if [ ! -f $name.log ]; then
    exit -1
fi
