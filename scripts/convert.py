#!/usr/bin/env python3

import re
import sys
import os


def convert(src, mathmode=False):

    result = src

    if mathmode:

        result = result.replace('\\','\\\\')
        result = result.replace('[','\[')
        result = result.replace(']','\]')
        result = result.replace('_','\_')
        result = result.replace('<','\<')
        result = result.replace('>','\>')


    result = result.replace('#','-')
    result = re.sub(re.compile('\A: *'), '', result)
    
    result = result.replace('<math>','$')
    result = result.replace('</math>','$')

    result = result.replace('{{eq}}','=')


    result = re.sub(re.compile('\A=(\s*[^=#]+\s*)='), r'# \1 ', result)
    result = re.sub(re.compile('\A==(\s*[^=#]+\s*)=='), r'## \1 ', result)
    result = re.sub(re.compile('\A===(\s*[^=#]+\s*)==='), r'### \1 ', result)
    result = re.sub(re.compile('\A====(\s*[^=#]+\s*)===='), r'#### \1 ', result)
    result = re.sub(re.compile('\A=====(\s*[^=#]+\s*)====='), r'##### \1 ', result)

    result = re.sub(re.compile('{{ *[vV]ariable *\|([^%=}|]+)\|([^%=}|]+)}}',re.VERBOSE), r'{{< variable "\1" >}}', result)
    result = re.sub(re.compile('{{ *[cC]ode *\|([^%=}|]+)\}}',re.VERBOSE), r'{{< code "\1" >}}', result)

    result = re.sub(re.compile('{{([^%}|<]*)}}'), r'{{< \1 >}}', result)
    result = re.sub(re.compile('{{([^%}|<]+)\|([^%}|]+)}}',re.VERBOSE), r'{{< \1 "\2" >}}', result)

    result = re.sub(re.compile('{{([^%}|<]+)\|([^%=}|]+)\|([^%=}|]+)}}',re.VERBOSE), r'{{< \1 "\2" "\3" >}}', result)
    result = re.sub(re.compile('{{([^%}|<]+)\|([^%=}|]+)=([^%=}|]+)\|([^%=}|]+)=([^%=}|]+)}}',re.VERBOSE), r'{{< \1 \2="\3" \4="\5" >}}', result)

    result = re.sub(re.compile('\[\[\s*Tutorial:([^|]+)\s*\|\s*([^|:]+)\]\]',re.VERBOSE), r'[\2](../\1)', result)
    result = re.sub(re.compile('\[\[\s*Manual:([^|]+)\s*\|\s*([^|:\s]+)\]\]',re.VERBOSE), r'{{< manual "\1" "\2" >}}', result)
    result = re.sub(re.compile('\[\[\s*Manual:([^|]+)\s*\]\]',re.VERBOSE), r'{{< manual "\1" "\1" >}}', result)


#    result = re.sub(re.compile('\[\[([^%}|<]+)\|([^%}|]+)\]\]',re.VERBOSE), r'[\2](\1)', result)

    result = re.sub(re.compile('\[\[Category:([\S ]*)\]\]'), '', result)

    result = re.sub(re.compile('\[\[[iI]mage:([\w.:_\- ]*)\|[\w\s|]+\|([\w\-\s.,:<>()/$]+)\]\]',re.VERBOSE), r'{{< figure src="/\1" width="500px" caption="\2" >}}', result)

    return result


name_orig = sys.argv[1]
name_split = name_orig.split(':')

category = name_split[0]
name = name_split[-1]

if category == 'Manual':
    if len(name_split)>2:
        sub_category = name_split[1]+'/'
    else:
        sub_category = ''
else:
    sub_category=''
    if len(name_split)>2:
        name = name_split[1] + ':_' + name_split[2]



source = open(name_orig, 'r')
#target = open('content/'+category+'/'+sub_category + name+'.md','w')
target = sys.stdout

references = dict()

categories = list()

codeblock = False
preblock = False
mathblock = False
articleblock = False
refblock = False

ol_index = 0
ol_block = False

footnote_index = 1

key_pair = '\|\s*(\w+?)\s*?=\s*([^|\n]+)\s*'

book_start = re.compile('\{\{book\s*', re.VERBOSE)

article_start = re.compile('\{\{article\s*', re.VERBOSE)
article_inline_end = re.compile(key_pair+'\}\}')
article_end = re.compile('\}\}')

ref_named_start = re.compile('<ref name="?([\w\s\-/]+)"?\s?>')
ref_start = re.compile('<ref>')
ref_end   = re.compile('</ref>')

lines = source.readlines()

for i in range(0,len(lines)):

    category_match = re.compile('\[\[Category:([\S ]*)\]\]')
    match = category_match.search(lines[i]) 
    if match:
        categories.append(match.group(1))

    if re.search(book_start, lines[i]):
        print(name+': book_start')
        articleblock = True
        lines[i] = re.sub(book_start, '{{< book ', lines[i])

    if re.search(article_start, lines[i]):
        print(name+': article_start')
        articleblock = True
        lines[i] = re.sub(article_start, '{{< article ', lines[i])

    if articleblock:
        lines[i] = re.sub(re.compile(key_pair), r'\1="\2" ', lines[i])
        
#        match = re.search(article_inline_end, lines[i])
#        if match:
#            print(name+': article_inline_end')
#            print(lines[i])
#            lines[i] = re.sub(article_inline_end, r'\1="\2" >}}', lines[i])
#            articleblock = False

        match = re.search(article_end, lines[i])
        if match:
            print(name+': article_end')
            articleblock = False
            lines[i] = re.sub(article_end, '>}}', lines[i], count = 1)



print('---',file=target)
print('title: "'+name.replace(category+':','').replace('_',' ').replace(':',': ')+'"',file=target)
if categories:
    print('tags: ["' + '", "'.join(categories) + '"]', file=target)
print('section: "'+category+'"', file=target)
print('---\n\n',file=target)


for line in lines:

    named_ref = re.compile('<ref name="?([\w\s\-/]+)"?\s*>([\w\s.()\-\'.,;]+)</ref>')
    match = re.search(named_ref, line)
    if match:
        print(name_orig+': '+match.group(0))
        footnote_label = match.group(1)
        references[match.group(1)] = match.group(2)
        line = re.sub(named_ref, r'[^\1]', line)

    line = re.sub(re.compile('<ref name="?([\w\s\-/]+)"?\s*/>'), r'[^\1]', line)

    if refblock:
        if re.search(ref_end, line):
            print(name+': ref_end')
            refblock = False
            line = re.sub(ref_end, '', line)
        else:
            references[footnote_label] += line
            line = ''


    match = re.search(ref_named_start, line)
    if match:
        print(name+': ref_named_start')
        refblock = True
        line = re.sub(ref_named_start, r'[^\1]', line)
        footnote_label = match.group(1)
        references[footnote_label] = ""

    if re.search(ref_start, line):
        print(name+': ref_start')
        refblock = True
        footnote_label = 'footnote-'+str(footnote_index)
        footnote_index+=1
        references[footnote_label] = ""
        line = re.sub(ref_start, '[^'+footnote_label+']', line)



    if line.strip() == '<math>':
        if not mathblock:
            mathblock = True
        line = line.replace('<math>','$$')

    if line.strip() == '</math>':
        if mathblock:
            mathblock = False
        line = line.replace('</math>','$$')

    line = convert(line, mathmode=mathblock)


    if '<pre>' in line:
        if not preblock:
            print("{{< code-block >}}",file=target)
        preblock = True
        line = line.replace('<pre>','')


    if not preblock and not mathblock and len(line)>0 and line[0] == ' ':
        if not codeblock:
            print("{{< code-block >}}",file=target)
        codeblock = True

    if '</pre>' in line:
        if preblock:
            print("{{< /code-block >}}",file=target)
        preblock = False
        line = line.replace('<pre>','')


    if not preblock and not mathblock and len(line)==0 or line[0] != ' ':
        if codeblock:
                print("{{< /code-block >}}",file=target)
        codeblock = False

    if line.strip() == '<ol>':
        print(name_orig+': start <ol> block.')
        ol_block = True
        ol_index = 0
        line = line.replace('<ol>', '')

    if line.strip() == '</ol>':
        print(name_orig+': end <ol> block.')
        ol_block = False
        line = line.replace('</ol>', '')

    if ol_block:
        if '<li>' in line:
            ol_index += 1
            line = line.replace('<li>', ' '+str(ol_index)+'. ')
        
        if '</li>' in line:
            line = line.replace('</li>', '')


    line = line.replace('**','  *')

    print( line, end='',file=target)

print('',file=target)

for ref in references.keys():
    print('[^'+ref+']: '+ references[ref], file=target)

source.close()
target.close()


