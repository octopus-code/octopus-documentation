#!/usr/bin/env bash

# set default values

name="undef"
usage="false"

# override defaults if enviroment variables are present:

if [[ -z "${OCTOPUS_TOP}" ]]; then
    OCTOPUS_TOP=$PWD/octopus
fi

if [[ -z "${OCTOPUS_SITE_PREFIX}" ]]; then
    OCTOPUS_SITE_PREFIX=""
fi

# process command line arguments:

while getopts d:n:p:h flag
do
    case "${flag}" in
        h) usage="true";;
        d) OCTOPUS_TOP=${OPTARG};;
        n) name=${OPTARG};;
        p) OCTOPUS_SITE_PREFIX=${OPTARG};;
    esac
done


if [ "$usage" = "true" ]; then
    echo "build-branch.sh builds the web-pages for a single branch of Octopus."
    echo "Command-line arguments:"
    echo "-d <octopus-directory>: Location of the Octopus sources (default: \$PWD/octopus)"
    echo "-n <name>: Branch display name for the web pages (default: custom)"
    echo "-h: this help message"
    exit;
fi


OCTOPUS_DOC=$OCTOPUS_TOP/doc/

pushd $OCTOPUS_TOP
branch=`git branch --show-current`
popd

if [ "$name" = "undef" ]; then
    name=$branch
fi

rm -f *.log
rm -f config_toml.end

grep -v '^#' branches | grep '\S' > branches_stripped

echo '[[menu.main]]' >> config_toml.end
echo '  name = "'$name'"' >> config_toml.end
echo '  branch = "'$branch'"' >> config_toml.end
echo '  weight = 1' >> config_toml.end
echo '' >> config_toml.end

echo "Building branch: " $branch $name

if [ -d $PWD/content/ ]; then
    rm -rf $PWD/content/*
else
    mkdir $PWD/content
fi

if [ ! -d $PWD/data/ ]; then
    mkdir $PWD/data
fi

if [ ! -d $PWD/default/includes ]; then
    mkdir -p $PWD/default/includes
fi

rm -rf static/images



cp -r $PWD/default/content/* $PWD/content/
#echo "copying: "$PWD/default/content/

if [ -d $OCTOPUS_DOC/html/content ];
then
    cp -r $OCTOPUS_DOC/html/content/* $PWD/content/
fi

cp -r $PWD/default/images $PWD/static/

if [ -d $OCTOPUS_DOC/html/images ];
then
    cp -r $OCTOPUS_DOC/html/images/* $PWD/static/images
fi

if [ -d $OCTOPUS_DOC/html/graph_data ];
then
    cp -r $OCTOPUS_DOC/html/graph_data $PWD/static/
else
    mkdir $PWD/static/graph_data
fi

cp -r $PWD/default/includes $PWD/

if [ -d $OCTOPUS_DOC/html/includes ];
then
    cp -r $OCTOPUS_DOC/html/includes/* $PWD/includes/
fi


#####################################################################################################

rm -rf content/Variables/*

./scripts/parse_variables.py -d data/varinfo.json --docdir=./content/ -s $OCTOPUS_TOP \
  --enable-json \
  --enable-untested --with-whitelist-untested=./scripts/whitelists/untested \
  --enable-undocumented --with-whitelist-undocumented=./scripts/whitelists/undocumented
./scripts/var2html.py --definitions=data/varinfo.json --variables='content/Variables' 
./scripts/classes.py -s $OCTOPUS_TOP/src/

sed "{s|OCTOPUSVERSION|$name|g;s|OCTOPUSBRANCH|$branch|g;s|SITE_PREFIX|$OCTOPUS_SITE_PREFIX|g}" config.toml_TEMPLATE > config.toml

cat config_toml.end >> config.toml

#####################################################################################################
echo "process #include_input:"

grep -r "^#include_input " content/ | sed '{s|:| |g}' > input_lines
grep -r "^#include_input " content/ | sed '{s|{{% ||g}' | cut -d ' ' -f 2 | sed '{s|\"||g}' > include_files
rm -rf static/testsuite

while IFS=' ' read -r testfile; do
    dir=`dirname $testfile`
    outfile=`basename $testfile`
    mkdir -p $PWD/static/$dir
    #echo   "process testfiles "$testfile, $dir, $outfile
    $PWD/scripts/annotate_input.py -d $PWD/data/varinfo.json -i $OCTOPUS_TOP/$testfile -o $PWD/static/$dir/$outfile
done < include_files

while IFS=' ' read -r mdfile marker testfile; do
    dir=`dirname $testfile`
    outfile=$dir/`basename $testfile`
    #echo   "process testfiles "$mdfile": "$marker" "$outfile
    sed -i -e "\|^$marker $testfile *$|r $PWD/static/$outfile" $mdfile
    sed -i -e "\|^$marker $testfile *$|d" $mdfile
done < input_lines

rm include_files input_lines 


#####################################################################################################
# Include files:
echo "process #include_file:"

grep -r "^#include_file " content/ | sed '{s|:| |g}' > general_input_lines
grep -r "^#include_file " content/ | sed '{s|{{% ||g}' | cut -d ' ' -f 2 | sed '{s|\"||g}' > include_general_files
rm -rf static/testsuite

while IFS=' ' read -r testfile; do
    dir=`dirname $testfile`
    outfile=`basename $testfile`
    mkdir -p $PWD/static/$dir
    #echo   "  process include files "$testfile, $dir, $outfile
    # cp $OCTOPUS_TOP/$testfile $PWD/static/$dir/$outfile
done < include_general_files

while IFS=' ' read -r mdfile marker testfile; do
    dir=`dirname $testfile`
    outfile=$dir/`basename $testfile`
    #echo   "   "$mdfile": "$marker" "$outfile
    sed -i -e "\|^$marker $testfile *$|r $OCTOPUS_TOP/$testfile" $mdfile
    sed -i -e "{s|^$marker $testfile *$||g}" $mdfile
done < general_input_lines

rm include_general_files general_input_lines 


#####################################################################################################
# Include tutorial images
echo "process #include_eps:"

grep -r "^#include_eps " content/ | sed '{s|md:|md |g}' > input_eps_lines

my_nproc=4
export OCTOPUS_TOP=$OCTOPUS_TOP
export PWD=$PWD

cat eps_input_lines | xargs -n 4 -P $my_nproc bash -c '
  dir=`dirname $2`
  outfile=`basename $2 eps`png
  if [ ! -f "$PWD/static/$dir/$outfile" ] || [ "$OCTOPUS_TOP/$2" -nt "$PWD/static/$dir/$outfile" ]; then
    mkdir -p $PWD/static/$dir
    convert -rotate 90 -flatten -density 300 -size 3300x2550 $OCTOPUS_TOP/$2 $PWD/static/$dir/$outfile
    #echo   "process eps files "$2, $dir, $outfile
  fi
'


while IFS=' ' read -r mdfile marker epsfile caption; do
    dir=`dirname $epsfile`
    outfile=/$dir/`basename $epsfile eps`png
    if [[ ! $caption == *"width"* ]]; then
        new_caption=$caption" width=\"500px\""
    else
        new_caption=$caption
    fi
    #echo   "process eps files "$mdfile": "$marker" "$outfile" |"$new_caption"|"
    sed -i -e "\|^$marker $epsfile $caption *$|c {{<figure src=\"$outfile\" $new_caption >}}" $mdfile
done < input_eps_lines


#####################################################################################################
# Include type definitions:
echo "process #include_type_def:"

grep -r "^#include_type_def" content/ | sed '{s|:| |g}' > def_input_lines
grep -r "^#include_type_def" content/ | cut -d ' ' -f 2 | sed '{s|\"||g}' > type_names

while IFS=' ' read -r typename; do
    sed -n "/^ *type.* $typename *$/,/end type/p" $OCTOPUS_TOP/src/*/*.F90 $OCTOPUS_TOP/src/*/*/*.F90 > $PWD/static/$typename.txt
done < type_names

while IFS=' ' read -r mdfile marker typename; do
    #echo   "process def "$mdfile": "$marker" "$typename
    sed -i -e "\|^$marker $typename *$|r $PWD/static/$typename.txt" $mdfile
    sed -i -e "{/^$marker $typename *$/d}" $mdfile
done < def_input_lines

rm type_names def_input_lines 


#####################################################################################################
# Include functions:
echo "process #include_function:"

grep -r "^#include_function" content/ | sed '{s|:| |g}' > function_input_lines
grep -r "^#include_function" content/ | cut -d ' ' -f 2 | sed '{s|\"||g}' > function_names

while IFS=' ' read -r functionname; do
    sed -n "/function *$functionname *(/,/end function/p" $OCTOPUS_TOP/src/*/*.F90 $OCTOPUS_TOP/src/*/*/*.F90 > $PWD/static/$functionname.txt
done < function_names

while IFS=' ' read -r mdfile marker functionname; do
    #echo   "process function "$mdfile": "$marker" "$functionname
    sed -i -e "\|^$marker $functionname *$|r $PWD/static/$functionname.txt" $mdfile
    sed -i -e "{/^$marker $functionname *$/d}" $mdfile
done < function_input_lines

rm function_names function_input_lines 


#####################################################################################################
# Include subroutines:
echo "process #include_subroutine:"

grep -r "^#include_subroutine" content/ | sed '{s|:| |g}' > subroutine_input_lines
grep -r "^#include_subroutine" content/ | cut -d ' ' -f 2 | sed '{s|\"||g}' > subroutine_names

while IFS=' ' read -r subroutinename; do
    sed -n "/subroutine *$subroutinename *(/,/end subroutine/p" $OCTOPUS_TOP/src/*/*.F90 $OCTOPUS_TOP/src/*/*/*.F90 > $PWD/static/$subroutinename.txt
done < subroutine_names

while IFS=' ' read -r mdfile marker subroutinename; do
    #echo   "process sub "$mdfile": "$marker" "$subroutinename
    sed -i -e "\|^$marker $subroutinename *$|r $PWD/static/$subroutinename.txt" $mdfile
    sed -i -e "{/^$marker $subroutinename *$/d}" $mdfile
done < subroutine_input_lines

rm subroutine_names subroutine_input_lines 

#####################################################################################################
# Include code marked snippets
echo "process #include_code_doc:"

grep -r "^#include_code_doc" content/ | sed '{s|:| |g}' > doc_input_lines
grep -r "^#include_code_doc" content/ | cut -d ' ' -f 2 | sed '{s|\"||g}' > doc_names

while IFS=' ' read -r docname; do
    sed -n "/^ *\!\# *doc_start *$docname *$/,/^ *\!\# *doc_end *$/p" $OCTOPUS_TOP/src/*/*.F90 $OCTOPUS_TOP/src/*/*/*.F90 | grep -v "doc_start" | grep -v "doc_end" > $PWD/static/doc_$docname.txt
done < doc_names

while IFS=' ' read -r mdfile marker docname; do
    #echo   "process code snippet "$mdfile": "$marker" "$docname
    sed -i -e "\|^$marker $docname|r $PWD/static/doc_$docname.txt" $mdfile
    sed -i -e "{s|^$marker $docname||g}" $mdfile
done < doc_input_lines

rm doc_names doc_input_lines

######################################################################################################
echo "process #include_input_snippet:"

grep -r "^#include_input_snippet " content/ | sed '{s|:| |g}' > input_snippet_lines

while IFS=' ' read -r mdfile marker input_file snippet_name; do
    #echo   "process input snippet "$mdfile": "$marker" "$input_file" "$snippet_name

    tmp_file=$PWD/static/`basename $input_file`_$snippet_name.txt
    #echo $tmp_name" "$input_file" "$snippet_name
    sed -n "/^ *\# *snippet_start *$snippet_name *$/,/^ *\# *snippet_end *$/p" $OCTOPUS_TOP/$input_file | grep -v "snippet_start" | grep -v "snippet_end" | $PWD/scripts/annotate_input.py -d $PWD/data/varinfo.json > $tmp_file

    sed -i -e "\|^$marker $input_file $snippet_name|r $tmp_file" $mdfile
    sed -i -e "\|^$marker $input_file $snippet_name|d" $mdfile

    rm $tmp_file
done < input_snippet_lines

rm input_snippet_lines 

rm -rf public/$OCTOPUS_SITE_PREFIX$name

echo "building pages in public/$OCTOPUS_SITE_PREFIX$name"

hugo --destination "public/$OCTOPUS_SITE_PREFIX$name"  > $name.log 2>&1

echo "Hugo exited with " $?


rm config.toml

rm config_toml.end
rm -rf content/*
rm -rf static/testsuite static/doc static/octopus static/includes
rm -rf static/*.txt
rm -rf static/images
rm -rf static/graph_data

