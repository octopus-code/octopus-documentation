---
Title: Support
---

### Support

{{<octopus>}} is a free and open source code.

We would like to acknowledge support for the test suite infrastructure, funding of schools, tutorials, as well as workshops and provision of resources for the hackathons from:

* <a href="https://mpsd.mpg.de/">Max Planck Institute for Structure and Dynamics of Matter</a>
{{< figure width="200px" alt="MPSD logo"   src="/images/mpsd-logo_full_ger_rgb-png.png">}}

* <a href="https://mpcdf.mpg.de">Max Planck Compute and Data Facility</a>
{{< figure width="200px" alt="MPCDF logo"  src="/images/mpcdflogo.png">}}

* <a href="https://www.simonsfoundation.org/flatiron/">Flatiron Institute</a>
{{< figure width="200px" alt="Flatiron Institute logo"  src="/images/Flatiron-Institute_blue-1.png">}}

* <a href="https://erc.europa.eu/homepage">European Research Council</a>
{{< figure width="200px" alt="ERC logo"  src="/images/ERC_logo.png">}}
