---
Title: "Octopus Features"
Weight: 2
---

{{< octopus >}} does many things in many different ways. This is a summary of what are the features that are currently implemented (some of them are only available in the development version).

## Theory 
* Density Functional Theory (DFT) for ground-state calculations
    - LDA, GGA, Meta-GGA and OEP functionals available through {{<libxc>}}
* Time-dependent DFT for response
* Independent particle approximation
* DFT+U
* Hartree-Fock and hybrid functionals

## Systems
* Works in 1, 2, and 3, and 4 dimensions
* Periodic boundary conditions in 1, 2, or 3 directions

## Linear Response
* Through time-propagation
    - Static polarizabilities
    - Absorption spectra (singlet, triplet)
* Mark Casida's formulation of response
    - Electronic excitations
* Sternheimer (frequency-dependent) linear-response formalism
    - Static and dynamic electric polarizabilities (including resonant response)
    - Static and dynamic electric first hyperpolarizabilities (including resonant response)
    - Static magnetic susceptibilities
    - Vibrational modes

## Non-linear response - Dynamics
* Ehrenfest dynamics for the nuclei
* Real-time TDDFT propagation
* Emission spectra, harmonic generation 
* Optimal control theory
* Car-Parrinello Molecular Dynamics
* Spin dynamics with collinear and noncollinear spins

## Technical
* Norm-conserving pseudopotentials (most commonly used formats)
* Real-space grid discretization (no basis sets)
* Curvilinear coordinates
* Parallelization in domains, states, and k-points.