---
title: "How to Cite Octopus"
weight: 10
---


{{< octopus >}} is a free program, so you have the right to use, change, distribute, and to publish papers with it without citing anyone (for as long as you follow the GPL license). However, the developers of {{< octopus >}} are also scientists that need citations to bump their CVs. Therefore, we would be very happy if you could cite one or more papers concerning {{< octopus >}} in your work. 

###  General Octopus papers  

These are the main references about Octopus, you should cite at least one of these in an article that uses Octopus.

* {{< article 
        title="Octopus, a computational framework for exploring light-driven phenomena and quantum dynamics in extended and finite systems" 
        authors="N. Tancogne-Dejean, M. J. T. Oliveira, X. Andrade, H. Appel, C. H. Borca, G. Le Breton, F. Buchholz, A. Castro, S. Corni, A. A. Correa, U. De     Giovannini, A. Delgado, F. G. Eich, J. Flick, G. Gil, A. Gomez, N. Helbig, H. Hübener, R. Jestädt, J. Jornet-Somoza, A. H. Larsen, I. V. Lebedeva, M. Lüders, M. A. L. Marques, S. T. Ohlmann, S. Pipolo, M. Rampp, C. A. Rozzi, D. A. Strubbe, S. A. Sato, C. Schäfer, I. Theophilou, A. Welden, A. Rubio" journal="The Journal of Chemical Physics" 
        year="2020" 
        volume="152" 
        pages="124119" 
        doi="10.1063/1.5142502" 
    >}}

* {{< article 
        title="Real-space grids and the Octopus code as tools for the development of new simulation approaches for electronic systems" 
        authors="X. Andrade, D. A. Strubbe, U. De Giovannini, A. H. Larsen, M. J. T. Oliveira, J. Alberdi-Rodriguez, A. Varas, I. Theophilou, N. Helbig, M. Verstraete, L. Stella, F. Nogueira, A. Aspuru-Guzik, A. Castro, M. A. L. Marques, and A. Rubio" 
        journal="Physical Chemistry Chemical Physics" 
        year="2015" 
        volume="17" 
        pages="31371-31396" 
        doi="10.1039/C5CP00351B" 
    >}}

* {{< article 
        title="octopus: a tool for the application of time-dependent density functional theory" 
        authors="A. Castro, H. Appel, Micael Oliveira, C.A. Rozzi, X. Andrade, F. Lorenzen, M.A.L. Marques, E.K.U. Gross, and A. Rubio" 
        journal="Phys. Stat. Sol. B" 
        volume="243" 
        pages="2465-2488" 
        year="2006" 
        doi="10.1002/pssb.200642067" 
    >}} 

* {{< article 
       title="octopus: a first-principles tool for excited electron-ion dynamics" 
       authors="M.A.L. Marques, Alberto Castro, George F. Bertsch, and Angel Rubio" 
       journal="Comput. Phys. Commun." 
       volume="151" 
       pages="60-78" 
       year="2003" 
       doi="10.1016/S0010-4655(02)00686-0" 
    >}}

###  Specific references about Octopus features  

If you use specific functionalities there are other references that you should cite as well. 

If you use the self-interaction correction (SIC) schemes for spinors:

* {{<article
     title="Self-interaction correction schemes for non-collinear spin-density-functional theory"
     authors="Nicolas Tancogne-Dejean, Martin Lueders, Carsten A. Ullrich"
     journal="Journal of Chemical Physics"
     volume="159"
     pages="224110"
     year="2023"
     doi="https://doi.org/10.1063/5.0179087"
    >}}

If you use the '''exciton wavefunction (TDTDM)''' part of the code:

* {{<article
     title="Time-Resolved Exciton Wave Functions from Time-Dependent Density-Functional Theory"
     authors="Jared R. Williams, Nicolas Tancogne-Dejean, Carsten A. Ullrich"
     journal="Journal of Chemical Theory and Computation"
     volume="17"
     pages="1795"
     year="2021"
     doi="https://doi.org/10.1021/acs.jctc.0c01334"
    >}}

If you use the '''magnon kick''' part of the code:

* {{< article 
            title="Time-Dependent Magnons from First Principles" 
            authors="Nicolas Tancogne-Dejean, Florian G.Eich, Angel Rubio" 
            journal="Journal of Chemical Theory and Computation" 
            volume="16" 
            pages="1007" 
            year="2020" 
            doi="https://doi.org/10.1021/acs.jctc.9b01064" 
        >}}

If you use the '''(TD)DFT+U''' part of the code:

* {{< article 
            title="Self-consistent $\mathrm{DFT}+U$ method for real-space time-dependent density functional theory calculations" 
            authors="Tancogne-Dejean, Nicolas and Oliveira, Micael J. T. and Rubio, Angel" 
            journal="Phys. Rev. B" 
            volume="96" 
            pages="245133" 
            year="2017" 
            doi="10.1103/PhysRevB.96.245133" 
        >}}

If you use the '''DFT+U+V''' part of the code:

* {{< article 
            title="Parameter-free hybridlike functional based on an extended Hubbard model: DFT + U + V" 
            authors="Tancogne-Dejean, Nicolas and Rubio, Angel" 
            journal="Phys. Rev. B" 
            volume="102" 
            pages="155117" 
            year="2020" 
            doi="10.1103/PhysRevB.102.155117" 
        >}}

If you use the '''parallel version''' of the code:

* {{< article 
            title="Time-dependent density-functional theory in massively parallel computer architectures: the octopus project" 
            authors="X. Andrade, J. Alberdi-Rodriguez, D. A. Strubbe, M. J. T. Oliveira, F. Nogueira, A. Castro, J. Muguerza, A. Arruabarrena, S. G. Louie, A. Aspuru-Guzik, A. Rubio, and M. A. L. Marques" 
            journal="J. Phys.: Cond. Matt." 
            volume="24" 
            pages="233202" 
            year="2012" 
            doi="10.1088/0953-8984/24/23/233202" 
        >}}

If you use the '''linear-response implementation''' you should cite:

* {{< article 
            title="Time-dependent density functional theory scheme for efficient calculations of dynamic (hyper)polarizabilities" 
            authors="Xavier Andrade, Silvana Botti, Miguel Marques and Angel Rubio" 
            journal="J. Chem. Phys" 
            volume="126" 
            pages="184106" 
            year="2007" 
            doi="10.1063/1.2733666" 
        >}}

If you use '''GPUs''':

* {{< article 
            title="Real-Space Density Functional Theory on Graphical Processing Units: Computational Approach and Comparison to Gaussian Basis Set Methods" 
            authors="X. Andrade and A. Aspuru-Guzik" 
            journal="J. Chem. Theo. Comput." 
            volume=""
            pages="" 
            year="2013" 
            doi="10.1021/ct400520e" 
        >}}

and if you use '''more efficient Poisson solvers''':

* {{< article 
            title="A survey of the parallel performance and accuracy of Poisson solvers for electronic structure calculations" 
            authors="P. García-Risueño, J. Alberdi-Rodriguez, M. J. T. Oliveira, X. Andrade, M. Pippig, J. Muguerza, A. Arruabarrena, A. Rubio" 
            journal="J. Comp. Chem" 
            volume="35" 
            pages="427–444" 
            year="2014" 
            doi="10.1002/jcc.23487"
        >}}

There is also a paper describing the '''propagation methods''' used in {{< octopus >}}:

* {{< article 
            title="Propagators for the time-dependent Kohn-Sham equations" authors="A. Castro, M.A.L. Marques, and A. Rubio" 
            journal="J. Chem. Phys" 
            volume="121" 
            pages="3425-3433" 
            year="2004" 
            doi="10.1063/1.1774980" 
        >}},

and a paper about {{<libxc>}}, the library used by octopus for the '''exchange-correlation functionals''',

* {{< article 
            title="Libxc: a library of exchange and correlation functionals for density functional theory" 
            authors="Miguel A. L. Marques, Micael J. T. Oliveira, and Tobias Burnus" 
            journal="Comput. Phys. Commun.," 
            volume="183" 
            pages="2272-2281" 
            year="2012" 
            doi="10.1016/j.cpc.2012.05.007" 
        >}} 
OAI: [arXiv:1203.1739](http://arxiv.org/abs/1203.1739)

###  General TDDFT references  

Finally, some general references on TDDFT, written by some of us:

* {{< book title="Fundamentals of time-dependent density functional theory" author="M.A.L. Marques, N.T. Maitra, F. Nogueira, E.K.U. Gross, and A. Rubio (Eds.)" publisher="Lecture Notes in Physics, Vol. 837, Springer, Berlin" year="2012" isbn="978-3-642-23518-4" url="http://www.springer.com/physics/theoretical%2C+mathematical+%26+computational+physics/book/978-3-642-23517-7" link="Springer">}}

* {{< book title="Time-dependent density functional theory" author="M.A.L. Marques, C. Ullrich, F. Nogueira, A. Rubio, K. Burke, and E.K.U. Gross (Eds.)" publisher="Lecture Notes in Physics, Vol. 706, Springer, Berlin" year="2006" isbn="978-3-540-35422-2" url="http://www.springer.com/home/generic/search/results?SGWID=7-40109-22-173664005-0" link="Springer">}} 

* {{< article title="Optical properties of nanostructures from time-dependent density functional theory" authors="Alberto Castro, M.A.L. Marques, Julio A. Alonso, and Angel Rubio" journal="J. Comp. Theoret. Nanoscience" volume="1" pages="231-255" year="2004" url="http://www.aspbs.com/html/a1304jct.htm " link="ASPBS">}} 

* {{< article title="Time-dependent density functional theory" authors="M.A.L. Marques and E.K.U. Gross" journal="Annu. Rev. Phys. Chem. " volume="55" pages="427-455" year="2004" url="http://arjournals.annualreviews.org/doi/abs/10.1146/annurev.physchem.55.091602.094449 " link="ARJournals">}} 

