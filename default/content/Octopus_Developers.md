---
title: Octopus developers team
weight: 20
---

{{% grid %}}
{{% item %}}
Current developers:
* Esra-Ilke Albar
* Xavier Andrade
* Heiko Appel
* Franco Bonafé
* Alex Buccheri
* Carlos Bustamante
* Alberto Castro
* Umberto De Giovannini
* Sebastian de la Peña Ruiz
* Hannes Huebener
* Christian Joens
* Martin Lang
* Ask Horth Larsen
* Irina Lebedeva
* I-Te Lu
* Martin Lueders
* Miguel A.L. Marques (hyllios)
* Henri Menke
* Fernando Nogueira
* Sebastian Ohlmann
* Micael Oliveira
* Carlo Andrea Rozzi
* Angel Rubio
* David Strubbe
* Iris Theophilou
* Francesco Troisi
* Simon Vendelbo Bylling Jensen
* Matthieu Verstraete
* Nicolas Tancogne-Dejean
{{% /item %}}

{{% item %}}
Former developers:
* Joseba Alberdi
* Fulvio Berardi
* Florian Buchholz
* Tilman Dannert
* Pablo García Risueño
* Johannes Flick
* Johanna Fuks
* Gabriel Gil
* Alain Delgado Gran
* Nicole Helbig
* Rene Jestaedt 
* Joaquim Jornet-Somoza
* David Kammerlander
* Kevin Krieger
* Cristian Le
* Guillaume Le Breton
* Florian Lorenzen
* Danilo Nitsche
* Roberto Olivares-Amaya
* Arto Sakko
* Ravindra Shinde
* José R. F. Sousa
* Axel Thimm
* Alejandro Varas
* Jessica Walkenhorst
* Davis Welakuh
* Jan Werschnik
* Philipp Wopperer
{{% /item %}}

{{% item %}}
{{< figure src="/images/P1280019.JPG" width="500px" caption="(former) developers of Octopus" >}}
{{% /item %}}
{{% /grid %}}