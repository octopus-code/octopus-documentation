---
title: "Releases"
---



These programs are distributed in the hope that they will be useful, but <b>without any warranty</b>; without even the implied warranty of <b>merchantability</b> or <b>fitness for a particular purpose</b>. Use them at your own risk!

##  Download Octopus  

Latest release: {{<releases-latest>}}

### All releases:

Each stable release is identified by two numbers (for example x.y). The first number indicates a particular version of Octopus and the second one the release. Before the 6.0 release (2016) Octopus used a three-number scheme, with the first two numbers being the version and the third one the release.

An increase in the revision number indicates that this release contains bug fixes and minor changes over the previous version, but that no new features were added (it may happen that we disable some features that are found to contain serious bugs).

Development versions (that you can get from the git repository) do not have a version number, they only have a code name. For code names we use the scientific names of Octopus species. These are the code names that we have used so far (this scheme was started after the release of version 3.2): 

{{< releases >}}

* Older releases
  * 1.4: [tar.gz](http://www.tddft.org/programs/octopus/down.php?file=1.4/octopus-1.4.tar.gz), 
        [i386 rpm](http://www.tddft.org/programs/octopus/down.php?file=1.4/octopus-1.4-1.i386.rpm), 
        [x86_64.rpm](http://www.tddft.org/programs/octopus/down.php?file=1.4/octopus-1.4-1.x86_64.rpm), 
        [src.rpm](http://www.tddft.org/programs/octopus/down.php?file=1.4/octopus-1.4-1.src.rpm), 
        [pdf](http://www.tddft.org/programs/octopus/download/1.4/octopus.pdf), 
        [ps](http://www.tddft.org/programs/octopus/download/1.4/octopus.ps), 
        [html.tar.gz](http://www.tddft.org/programs/octopus/download/1.4/octopus.html.tar.gz), 
        [html](http://www.tddft.org/programs/octopus/download/1.4/octopus.html)
  * 1.3: [tar.gz](http://www.tddft.org/programs/octopus/down.php?file=1.3/octopus-1.3.tar.gz), 
        [i386.rpm](http://www.tddft.org/programs/octopus/down.php?file=1.3/octopus-1.3-1.i386.rpm), 
        [fedora.i386.rpm](http://www.tddft.org/programs/octopus/down.php?file=1.3/octopus-1.3-1.fedora.i386.rpm), 
        [src.rpm](http://www.tddft.org/programs/octopus/down.php?file=1.3/octopus-1.3-1.src.rpm), 
        [pdf](http://www.tddft.org/programs/octopus/download/1.3/octopus.pdf), 
        [ps](http://www.tddft.org/programs/octopus/download/1.3/octopus.ps), 
        [html.tar.gz](http://www.tddft.org/programs/octopus/download/1.3/octopus.html.tar.gz), 
        [html](http://www.tddft.org/programs/octopus/download/1.3/octopus.html/octopus_toc.html)
  * 1.1: [.tar.gz](http://www.tddft.org/programs/octopus/down.php?file=1.1/octopus-1.1.tar.gz), 
        [i386.rpm](http://www.tddft.org/programs/octopus/down.php?file=1.1/octopus-1.1-1.i386.rpm), 
        [src.rpm](http://www.tddft.org/programs/octopus/down.php?file=1.1/octopus-1.1-1.src.rpm), 
        [pdf](http://www.tddft.org/programs/octopus/download/1.1/octopus.pdf), 
        [ps](http://www.tddft.org/programs/octopus/download/1.1/octopus.ps), 
        [html.tar.gz](http://www.tddft.org/programs/octopus/download/1.1/octopus.html.tar.gz), 
        [html](http://www.tddft.org/programs/octopus/download/1.1/octopus.html/octopus_toc.html)
  * historical: 
        [1.0.1.tar.gz](http://www.tddft.org/programs/octopus/down.php?file=1.01/octopus-1.0.1.tar.gz), 
        [1.0.tar.gz](http://www.tddft.org/programs/octopus/down.php?file=octopus-1.0.tar.gz), 
        [0.9.tar.gz](http://www.tddft.org/programs/octopus/down.php?file=octopus-0.9.tar.gz)

You can also get the development version ({{<devel_name>}}) from [Gitlab](https://gitlab.com/octopus-code/octopus).

## Live images containing {{<octopus>}}

The following images iso-images for a system with an installation of {{<octopus>}} can be downloaded here:

* [Octopus 12.1](https://theory.mpsd.mpg.de/octopus/iso-images/octopus-12.1_amd64.iso) (md5sum: 30d884cc012470db379273409428e525)

Live ISO username: octopus<br>
Live ISO password: octopus

The image contains an installation of [postopus](https://gitlab.com/octopus-code/postopus) and a quick start example.
The example can be launched in a terminal via
```bash
octopus@debian-live:~$ jupyter-notebook Quick_Start.ipynb
```

##  LibXC  

[Libxc](http://www.tddft.org/programs/libxc) is the set of exchange-correlation functionals used in {{< octopus >}}, which is also a separate library shared among several codes. Download [here](http://www.tddft.org/programs/libxc/download/).

##  Other packages  

* [xyz-tools.tar.gz](http://www.tddft.org/programs/octopus/down.php?file=xyz-tools.tar.gz): Some handy tools to generate clusters, fullerenes, nanotubes and manipulate <tt>.xyz</tt> files.

* [FortranCL](http://code.google.com/p/fortrancl/): a OpenCL Fortran 90 interface. Based on Octopus OpenCL implementation.

* [carbon-tb.tar.gz](http://www.tddft.org/programs/octopus/down.php?file=carbon-tb.tar.gz): Tight-binding code for carbon. You can calculate phonons, dynamics, etc.

* [oct_parser-1.0.tar.gz](http://www.tddft.org/programs/octopus/down.php?file=oct_parser-1.0.tar.gz), 
  [oct_parser-1.1.tar.gz](http://www.tddft.org/programs/octopus/down.php?file=oct_parser-1.1.tar.gz): The parser used in octopus. Feel free to use it in your own projects. More info on the parser is available at {{< manual "Basics/Input_file" "Input_file" >}}.
