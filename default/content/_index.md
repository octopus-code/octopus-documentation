---
title: "Octopus Documentation Homepage"
author: "Martin Lueders"
section: "Octopus"
weight: 1
---

# Octopus

{{< octopus >}} is a scientific program aimed at the ab initio virtual experimentation on a hopefully ever-increasing range of system types. Electrons are described quantum-mechanically within density-functional theory (DFT), in its time-dependent form (TDDFT) when doing simulations in time. Nuclei are described classically as point particles. Electron-nucleus interaction is described within the pseudopotential approximation. Octopus can also treat light-matter interaction at the level of Maxwell-TDDFT or quantum electrodynamics DFT (QEDFT).


For optimal execution performance {{< octopus >}} is parallelized using MPI and OpenMP and can scale to tens of thousands of processors. It also has support for graphical processing units (GPUs) through OpenCL, CUDA and HIP.

{{< octopus >}} is free software, released under the GPL license, so you are free to download it, use it and modify it.


# News:

Latest release: {{<releases-latest>}}

# Octopus mailing lists:

The {{<octopus>}} community utilizes three mailing lists:

* octopus-announce@lists.octopus-code.org: any kind of announcements, e.g. new release, workshops, etc.
* octopus-users@lists.octopus-code.org: getting help regarding the use of {{<octopus>}}
* octopus-devel@lists.octopus-code.org: closed list for discussions amongst developers.

In order to sign up with one or more of these mailing lists, visit:
* https://listserv.gwdg.de/mailman/listinfo/octopus-announce
* https://listserv.gwdg.de/mailman/listinfo/octopus-users
* https://listserv.gwdg.de/mailman/listinfo/octopus-devel

# Development

* The [testsuite app](https://octopus-code.org/testsuite/)
* The {{<octopus-doxygen>}}
* The [source code documentation](https://octopus-code.org/doc/main/doxygen_doc/index.html)

# Useful info:

* {{< versioned-link "Citing_Octopus" "How to cite Octopus" >}}
* {{< versioned-link "Books" "Books" >}}
* {{< versioned-link "Links" "Links" >}}
* {{< versioned-link "FAQ" "Frequently Asked Questions" >}}

# Related projects

* [libxc](https://libxc.gitlab.io/)
* [fortrancl](https://code.google.com/p/fortrancl)
* [ape](https://ape.gitlab.io/ape/)
* [Postopus](https://octopus-code.gitlab.io/postopus/index.html)