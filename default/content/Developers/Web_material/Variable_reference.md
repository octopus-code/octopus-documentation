---
Title: "Variable desription"
Weight: 50
---

The {{< versioned-link "variables" "reference section" >}} of the {{< octopus >}} input variables is automatically generated from the source code.

### Source documentation

The syntax for the annotation in the source code can be seen in the following example.
```Fortran
    !%Variable {{< variable "PropagationSpectrumTransform" >}}
    !%Type integer
    !%Default sine
    !%Section Utilities::oct-propagation_spectrum
    !%Description
    !% Decides which transform to perform, if <tt>SpectrumMethod = fourier</tt>.
    !%Option sine 2
    !% Sine transform: <math>\int dt \sin(wt) f(t)</math>. Produces the imaginary part of the polarizability.
    !%Option cosine 3
    !% Cosine transform: <math>\int dt \cos(wt) f(t)</math>. Produces the real part of the polarizability.
    !%Option laplace 1
    !% Real exponential transform: <math>\int dt e^{-wt} f(t)</math>. Produces the real part of the polarizability at imaginary
    !% frequencies, <i>e.g.</i> for Van der Waals <math>C_6</math> coefficients.
    !% This is the only allowed choice for complex scaling.
    !%End
    call parse_variable(namespace, 'PropagationSpectrumTransform', SPECTRUM_TRANSFORM_SIN, spectrum%transform)
    if(.not.varinfo_valid_option('PropagationSpectrumTransform', spectrum%transform)) then
      call messages_input_error(namespace, 'PropagationSpectrumTransform')
    end if
    call messages_print_var_option(stdout, 'PropagationSpectrumTransform', spectrum%transform)
```
This description will be used by the {{< manual "utilities:oct-help" "oct-help" >}} command, and also for the documentation web pages.
{{< notice note >}}
Note that LaTeX commands can be used in the description text. See  {{<versioned-link "Developers/Web_material/Writing_tutorials/#including-mathematical-formulas" "here" >}} for details.
{{< /notice >}}

### Using the python scripts

When building the documentation, a set of python scripts are parsing the source code, as well as the input files of the testsuite and the markdown files of the manual and the tutorials to generate a comprehensive database of the variables, including references to the source, testfiles and the tutorials, which is written to the {{< file "varinfo.json" >}} file. 

{{% expand "JSON entry for this example" %}}
```json
{
  "propagationspectrumtransform": {
    "CallLine": "call parse_variable(namespace, 'PropagationSpectrumTransform', SPECTRUM_TRANSFORM_SIN, spectrum%transform)",
    "Default": [
      "sine"
    ],
    "Description": [
      "Decides which transform to perform, if <tt>SpectrumMethod = fourier</tt>."
    ],
    "LineNumber": 233,
    "Manuals": [
      "External_utilities/oct-propagation_spectrum"
    ],
    "Name": "PropagationSpectrumTransform",
    "Options": [
      {
        "Description": [
          "Sine transform: <math>\\int dt \\sin(wt) f(t)</math>. Produces the imaginary part of the polarizability."
        ],
        "Name": "sine",
        "Value": "2"
      },
      {
        "Description": [
          "Cosine transform: <math>\\int dt \\cos(wt) f(t)</math>. Produces the real part of the polarizability."
        ],
        "Name": "cosine",
        "Value": "3"
      },
      {
        "Description": [
          "Real exponential transform: <math>\\int dt e^{-wt} f(t)</math>. Produces the real part of the polarizability at imaginary",
          "frequencies, <i>e.g.</i> for Van der Waals <math>C_6</math> coefficients.",
          "This is the only allowed choice for complex scaling."
        ],
        "Name": "laplace",
        "Value": "1"
      }
    ],
    "Section": "Utilities::oct-propagation_spectrum",
    "Sourcefile": "td/spectrum.F90",
    "Testfiles": [
      "real_time/12-absorption.07-spectrum_cosine.inp",
      "real_time/12-absorption.08-spectrum_exp.inp"
    ],
    "Tutorials": [],
    "Type": "integer"
  }
}
```
{{% /expand %}}

This JSON file is then used to generate the input variable web pages. The information is also used by the script {{< file "annotate_input.py" >}}, which is automatically invoked by the {{< code "#include_input" >}} directive, but can also be called manually, if a part of an input file should be included in some tutorial markdown file.


### Automatic generation of links

The script, which parses the source files, also parses the testsuite input files and the tutorial files.
These, together with a link to the source file in which the variable is parsed, are them added to the variable description pages. 
