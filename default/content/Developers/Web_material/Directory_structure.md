---
Title: "Directory structure"
weight: 6
---

The {{< octopus >}} documentation system allows to maintain the documentation for various versions of {{< octopus >}} simultaneously.
This, of course, requires that the some organization of the documentation source (markdown, images, etc.) files, which will be explained in this chapter.

Let's assume that we the {{< hugo >}} top directory is {{< file "$HUGO_DOC" >}}.

{{< hugo >}} usually has the following [directory structure](https://gohugo.io/getting-started/directory-structure/):

* {{< file "content/" >}}: 
    - {{< file "Manual/" >}}: 
    - {{< file "Tutorial/" >}}: 
    - {{< file "Developers/" >}}: 
* {{< file "static/" >}}: \
    here are images, style sheets, etc.
* {{< file "data/" >}}: \
    data to be used in shortcodes and templates
* {{< file "resources/" >}}: \
    other resources
* {{< file "layouts/" >}}: \
    shortcodes
* {{< file "themes/" >}}: \
    all files related to the theme. Some are overwritten by contents of {{< file layouts >}} or {{< file static >}}.
* {{< file "archetypes/" >}}: \
    templates for generating new pages.


In our case, the following files can be version-dependent:

* {{< file "content/" >}}
* {{< file "static/images/" >}} 

Therefore, they should be part of the {{< octopus >}} code repository. They should reside in:

* {{< code-inline >}}{{< basedir >}}/doc/html/content/{{< /code-inline >}}
* {{< code-inline >}}{{< basedir >}}/doc/html/images/{{< /code-inline >}}

If these folders are present in a given branch of the code repository, the {{< file build-pages.sh >}} script will copy their content into the respective {{< hugo >}} folders.
If they do not exist in the code repository, they will be copied from 
* {{< file "$HUGO_DOC/octopus-documentation/default/content/" >}}
* {{< file "$HUGO_DOC/octopus-documentation/default/images/" >}}

{{< notice note >}}
The version selection of the three sections {{< file "Manual" >}}, {{< file "Tutorial" >}} and {{< file "Developers" >}} are independent. It is therefore possible to have a version dependent tutorial section, while using the defaults for the rest.
{{< /notice >}}