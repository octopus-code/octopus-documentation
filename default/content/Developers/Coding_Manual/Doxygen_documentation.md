---
title: "Doxygen documentation"
section: "Developers"
weight: 10
authors: "Martin Lueders, Alex Buccheri"
---

### Online doxygen documentation

The doxygen documentation for the {{<code "main">}} branch of Octopus can be found [here](https://www.octopus-code.org/doc/main/doxygen_doc/). It is automatically generated at a push to the main branch.

{{<notice "note">}}
Currently, the links to the source code are broken and will most likely point to a wrong part of the file.
{{</notice>}}

### Running doxygen locally

To build the doxygen pages locally, go into the folder {{<file "doc/doxygen/">}} inside the {{<octopus>}} tree, and run the command 
```shell
./create_documentation.sh
```
 This might take a few minutes to complete. In case you need to rebuild the pages more often while writing new documentation, you might want to disable the creation of call graphs, by hanging the line
```
HAVE_DOT = YES
```
to 
```
HAVE_DOT = NO
```
in the file {{<file "Doxyfile">}}. 


### Writing documentation

There are several ways how to include comments into the Fortran source, which are shown in the following code snippet: 
```Fortran
module routines

  !> @class DummyType
  !! @brief Dummy class, demoing Doxygen documentation syntax.
  !!
  !! ### Notes:
  !! * Markdown syntax is completely valid
  !! * Doxygen will infer public/private based on the declarations
  !! * However, it will get this wrong if the default behaviour is changed, i.e. one declares the class
  !!   attributes private by default
  !<
  type DummyType
     !> A private attribute will not be parsed by doyxgen
     integer, private :: a
     !> @public An example of public data
     real(dp), public :: b
     integer, public :: c   !< @public Another example of public data, with same-line doc
     !> @private Attributes tagged as private will also not be shown in the documentation
     integer :: d
     integer :: e   !< Assumed public
   contains
     procedure :: start => dummy_start  !< @copydoc routines::dummy_start
     final :: end                       !< @copydoc routines::end
  end type DummyType

contains

  !> @brief A dummy initialiser for the DummyType class.
  !!
  !! @param[in]  a  Some integer
  !! @param[in]  b  Some double
  !<
  subroutine dummy_start(this, a, b)
    class(DummyType), intent(inout) :: this
    integer, intent(in) :: a
    real(dp), intent(in) :: b
    this%a = a
    this%b = b
    this%c = a * b
  end subroutine dummy_start
```

