---
Title: "Octopus Developers Meeting 2019"
weight: 20
---


### Octopus developers workshop - 11-12 June 2019 
The fourth Octopus developers workshop will take place at the Max Planck Institute for the Dynamics and Structure of Matter, in Hamburg, Germany from the 11 to the 12 of June 2019.

#### List of participants

- Angel Rubio
- Micael Oliveira
- Martin Lueders
- Nicolas Tancogne-Dejean
- Florian Eich (Tuesday 100%, Wednesday 50%)
- Alberto Castro
- Sebastian Ohlmann
- Heiko Appel
- Shunsuke Sato
- Florian Buchholz
- Xavier Andrade
- Carlo Andrea Rozzi
- Umberto De Giovannini
- Hannes Huebener
- Enrico Ronca
- Lukas Windgaetter
- Dong bin Shin
- Christian Schaefer
- Franco Bonafé
- Iris Theophilou
- Henning Glawe
- Davis Welakuh

{{< figure src="images/IMG_20190611_194517.jpg" width="600px" >}}

#### Program

|         | *Tuesday*  | *Wednesday* |
| :--:    |:---        |:----        |
|**Morning**  | 09:00 Welcome (Heiko) | 09:00 Sebastian     |
|         | 09:10 Micael          | 09:50 Xavier        |
|         | 10:30 Coffee break    | 10:30 Coffee break  |
|         | 11:00 Davis           | 11:00 Nicolas       |
|         | 11:20 Franco          | 11:10 Alberto       |
|         | 11:40 Heiko           | 11:50 Carlo         |
|         | 12:00 Lunch           | 12:30 Lunch         |
|         |                       |                     |
|**Afternoon** | 13:30 Xavier        |  13:30 Discussions               |
|         | 14:00 Florian E.      |  15:30 Coffee break              | 
|         | 14:30 Umberto/Hannes  |  16:00 Final remarks/Conclusions |
|         | 15:00 Nicolas         |  17:00 Departure                 |         
|         | 15:30 Florian B.      |                                  |         
|         | 16:00 Coffee break    |                                  |
|         | 16:30 Discussions     |                                  |
|         |                       |                                  |
| **Evening** | Workshop dinner      |                                  |


Talks:
- State of the <del>Union</del>Octopus (Micael)
- Non-linear transport from real-time TDDFT (Xavier)
- Magnons from real-space real-time TDDFT (Florian Eich)
- TBA (Davis)
- Twisted light and local field enhancement (Franco)
- Time-dependent density-functional theory for QED: coupled Ehrenfest-Maxwell-Pauli-Kohn-Sham equations (Heiko Appel)
- Floquet analysis with Octopus (Umberto/Hannes)
- Hybrid functionals and real-time TDDFT in Octopus (Nicolas Tancogne-Dejean)
- Conjugate Gradients Algorithm for RDMFT (Florian Buchholz)
- Improving ground-state calculations, analyzing testsuite results (Sebastian Ohlmann)
- Octopus and GPUs (Xavier)
- Working with solids in Octopus: The Delta factor strikes back (Nicolas Tancogne-Dejean)
- New utilities in Octopus (Nicolas Tancogne-Dejean)
- Commutator-free Magnus expansions for octopus (Alberto)
- From static PCM to real-time PCM in octopus (Carlo)

#### Discussions 

These are topics we should discuss during the meeting.

- Next Octopus developers workshop
- Scheduling a monthly meeting
- Upcoming Octopus paper
- Compiler warnings in buildbot tests.
- Deprecation of forall in the Fortran standard.
- Proposal to make components of derived data types private by default.
- Proposal to reduce the default values for the {{< variable "ConvRelDens" >}} and {{< code "EigensolverTol" >}} input options.
- Introduction of Fortran 2003/2008 OOP features.
- Permissions on gitlab group and associated projects.
- New release manager.
- Proposal to make reviews of merge requests mandatory
- Modular parts interchangeable with other codes


#### Minutes

- **Next Octopus developers workshop**
  - Looking for volunteers to organize the next workshop.
  - Location and dates still to be decided. Suggestions for location: NYC, Jena.
- **Scheduling a monthly meeting**
  - We will have a monthly video-conference meeting the first Monday of each he month, at 6 PM (CET/CEST)
- **Upcoming Octopus paper**
  - Nicolas: Currently gathering contributions. All contributions should be sent before the end of June.
  - Micael: Paper will be submitted to NPJ Computational Materials. Length is not a problem, but possible lack of novelty might be one.
- **Compiler warnings in buildbot tests.**
  - The compilation on the buildbot builders produces a lot of compiler warnings. Some of them are useful and should not be discarded, but they are drowned in a lot of useless warnings.
  - Compiler flags for warnings should be restricted to the useful ones and the presence of warnings should make the build fail. The failure should come from Buildbot itself, not the compiler, so that we get all the warnings for the full compilation of the code.
  - To decide the subset of useful warnings we first need to check what warnings we are getting and if we can actually remove them.
- **Deprecation of forall in the Fortran standard.**
  - The forall statement has been declared obsolete in the Fortran 2018 standard. From our own experience, forall statements are not really faster than do loops and can even be slower when iterating over more than one index.
  - Thus no new forall statements should be added to the code and the existing ones should be replace by do loops.
- **Proposal to make components of derived data types private by default.**
  - There is currently no simple and reliable way of checking if a component of a derived data type is used outside the module where the type is defined (there are many components with identical names and the names of the type instances are arbitrary).
  - The proposal is to use the same scheme as for the private/public statements in modules: by default all the components of a derived data type are declared private and the components that are used outside the module are explicitly declared to be public. An exception to this rule is if all or the almost all the components of a derived data type are public, in which case a comment stating that components are public by default is added and the the private components are indicated explicitly.
- **Proposal to reduce the default values for the ConvRelDens and EigensolverTol input options.**
  - The current default values for {{< variable "ConvRelDens" >}} (1e-5) and {{< code "EigensolverTolerace" >}} (1e-6) tend to produce differences in total energies and other quantities between identical runs performed on different machines or with different parallelization strategies that are higher than what one is usually aiming at.
  - After some discussion about the best way to tackle this, the following suggestion was agreed on:
    - Reduce ConvRelDens default to 1e-6 and {{< code "EigensolverTolerace" >}} default to 1e-7
    - Change the default for ConvEnergy from 0 (not used) to some suitable value (0.1 milliHartree?)
    - Change the SCF stopping criteria such that the code exits the SCF loop only if the convergence criteria are met two consecutive iterations
- **Introduction of Fortran 2003/2008 OOP features.**
  - Compiler support for Fortran 2003 OOP features is now quite mature. 
  - These features would be very useful for the planned multi-system support. 
  - Need to ensure that these are correctly supported by all compilers of interest.
- **Permissions on gitlab group and associated projects.**
  - There is no policy about the group/project to which new members are added and the corresponding permissions. 
  - Currently the senior developers are part of the octopus-code group, which gives them access to all the projects within the group. By default other developers are added to the octopus-code/octopus project. Developers are added to the octopus-code/octopus-private project and to the buildbot subgroup projects upon request.
  - It was agreed to make this our official policy regarding gitlab user permissions.
- **New release manager.**
  - Micael has been doing the release for almost five years.
  - It would be good if the associated workload was shared with the other developers.
  - There is currently no one else with experience in doing the releases with git and gitflow.
  - It was agreed to rotate the role of release manager among the developers, such that one person takes the role for the duration of one release cycle (one year, roughly). This should be done with the support of the previous release manager, so that the knowledge is smoothly passed on.
  - Martin has volunteered to be the new release manager. 
- **Proposal to make reviews of merge requests mandatory**
  - Merge request reviews have become an important tool to improve the code quality and in preventing the introduction of bugs. Most merge request are currently reviewed, but not all. The proposal is to make them mandatory by using the approval system of gitlab.
  - Concerns were raised that this could slow down development or that it could become a bureaucratic procedure.
  - There was an agreement that reviews should be performed in a reasonable amount (eg. one week) of time and developers should be able to go on with the merge if this time is exceeded.
  - Code reviews should definitely not be a bureaucratic procedure. The purpose of the code review is to improve the code in a constructive way, not to hinder development.
  - Code reviewers should look for things like code correctness, compliance with coding standards, existence and appropriateness of regression tests, etc.
- **Modular parts interchangeable with other codes**
  - Xavier will be part of a project that will develop code in C++. Some of it could be reused in Octopus. There were no objections to this.
  - Using git subtrees could make it easier to share code with other projects. We could already try to do this with the Octopus parser.
- **Git workflow**
  - Micael presented the current workflow used (gitflow with a couple of tweaks, mainly regarding the hotfix releases). 
  - Advantages, disadvantages and pitfalls of the current workflow were discussed. The main problem seems to be the necessity of branching hotfixes from the master branch, instead of develop. 
  - A compromise was found by allowing developers to create the bugfix branches from develop and let the release manager handle any required rebase. 
  - If a bug fix is to be applied to master, but not to develop, then this needs to be clearly indicated in the corresponding merge request.
- **Octopus webpage**
  - The webpage starts to show its age. The wiki is not really useful anymore, as there have been very few contributions from external users over the last couple of years.
  - An alternative would be to use a markdown based static page, as done for Libxc. This would allow to automatize some of the content updates. It would also allow to distribute some of the contents, like the manual, with the code sources.
  - The "Articles published with Octopus" page on the wiki is not up to date. Maybe it would be better to delete the page. On the other hand, the "How do I cite Octopus?" page should be always up to date.

#### Practical information 

The meeting will be at the [MPSD](https://goo.gl/maps/pVDNKmmgEDK2) in Seminar Room SR I. You can find information how to reach it [here](http://www.mpsd.mpg.de/en/institute/directions).

