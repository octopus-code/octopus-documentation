---
title: "Meeting 2016"
weight: 10
---


##  Octopus developers meeting - 9-11 November 2016  
The third Octopus developers meeting took place at the Max Planck Institute for the Dynamics and Structure of Matter, in Hamburg, Germany from the 9 to the 11 of November 2016.

###  List of participants  
- Angel Rubio
- Micael Oliveira
- Nicolas Tancogne-Dejean
- Shunsuke A. Sato
- Thibault J.-Y. Derrien
- Christian Schaefer
- Umberto De Giovannini
- Philipp Wopperer
- Lorenzo Stella
- Theophilou Iris
- Florian Eich
- Hannes Huebener
- Soeren Nielsen
- Matthieu Verstraete (99.99999% chance)
- Miguel Marques (not for the 3 days... I'll let you know later)
- Gabriel Gil (he will be developing next year with Carlo)
- Carlo A. Rozzi
- Kyung-Min Lee
- Johannes Flick
- Rene Jestaedt
- Nicole Helbig (until Thursday evening)
- Norah Hoffmann
- Thomas Brumme
- Fabio Covito
- Heiko Appel
- Raison Dsouza
- Cesar A. Rodriguez Rosario

###  Program  

|         | Wednesday  | Thursday  | Friday |
| :--:    | :---       | :---      | :---   |
|**Morning**  | 10:00 Angel (Welcome) | 09:00 Christian      | 09:00 Coding/Discussions |
|         | 10:10 Micael          | 09:30 Johannes       |                          |
|         | 10:30 Coffee break    | 10:00 Rene           | 10:30 Coffee break       |
|         | 11:00 Discussion      | 10:30 Coffee break   |                          |
|         |                       | 11:00 Heiko          | 11:00 Coding/Discussions |
|         |                       | 12:00 Shunsuke       |                          |
| **Afternoon** | 13:30 Carlo              | 13:30 Nicolas        | Departure |
| | 13:50 Fabio              | 14:00 Thibault       | |
| | 14:10 Iris               | 14:30 Phillip        | |
| | 14:40 Coding/Discussions | 15:00 Lorenzo        | |
| | 16:00 Coffee break       | 16:00 Coffee break   | |
| | 16:30 Coding/Discussions | 17:00 Brainstorming: The future of Octopus | |
| **Evening** | | Workshop dinner | |

### Talks:
- Micael Oliveira, "Migration to git, gitlab, and the future of the testfarm"
- Philipp Wopperer, "Methods for calculation of photoemission observables with octopus (t-surff)"
- Hannes Huebener, "Pump-probe in solids"
- Shunsuke A. Sato, "Lattice dynamics + Comparison with Yabana group's program"
- Nicolas Tancogne-Dejean, "Working with solids in Octopus"
- Iris Theophilou, "Basis set implementation in Octopus and current stage of RDMFT development"
- Fabio Covito, "A novel non-equilibrium Green’s function approach to carriers dynamics in many body interacting systems"
- Carlo A. Rozzi, "From PCM to real-time PCM: we have a cunning plan"
- Johannes Flick, "Cavity-QED beyond model systems and OEP in octopus"
- Lorenzo Stella, "Ideas for computational nanoplasmonics using OCTOPUS: Jellium pseudo-potential generator and projected equations of motion"
- Rene Jestaedt, "TBA"
- Heiko Appel, "In-situ visualization in octopus, open problems in QEDFT, outlook and wishlist"
- Thibault J.-Y. Derrien, "Defining the excited electron density and the hole density in TD-DFT"
- Christian Schaefer, "TBA"

###  Practical information  
The meeting will be at the [MPSD](https://goo.gl/maps/pVDNKmmgEDK2) in Seminar Room SR V. You can find information how to reach it [here](http://www.mpsd.mpg.de/en/institute/directions).
