---
title: "Videoconference meeting autumn 2015"
weight: 9
---



Doodle poll to set the meeting date:

[http://doodle.com/poll/grw5nq6ccqp64n6t](http://doodle.com/poll/grw5nq6ccqp64n6t)

##  Topics to discuss  

- Release of 5.0.x
- [http://www.tddft.org/programs/octopus/wiki/index.php/Developers:Votes:Namespace Module naming convention]
- Move to hg/git?
- Status of the Frozen Density Embedding (FDE) code
- Code changes
  - Change default mixing to the potential to avoid warnings about "negative density after mixing".
  - Assume all XYZ are in Angstrom units. PDB, XSF?
- Domain name: octopus.tech, octopus-code.org, other options?
- Another code named Octopus: [https://github.com/STEllAR-GROUP/octopus](https://github.com/STEllAR-GROUP/octopus).
- Status of the test suite and test farm.
- Fortran 2003 object-oriented features like inheritance and type-bound procedures. Current issue: lack of support in G95, old pathscale (already not being used), and old gfortran.
- Social media presence? [https://www.facebook.com/octopus.code](https://www.facebook.com/octopus.code)

##  Present  

Alberto, David, Fernando, Hannes, José Rui, Matthieu, Micael, Xavier

##  Meeting minutes  

* Release of 5.0.x
  * All major problems seem to be fixed. There are known problems in the test farm, but these are not affecting the results given by the code. 
  * Micael and David will go through the [[Developers:Preparing Release]] page.
  * All the developers should make sure the following pages are up to date: {{<versioned-link "Releases/Changes" "Changes" >}} and {{< manual "Updating to a new version" "Updating to a new version" >}}.

* Module naming convention
  * Options 3 and 4 from {{< code "Namespace" >}} were considered the best candidates.
  * Voting between those two options ended up in a tie. Developers should add pros and cons for both options to the wiki page and a new vote will be organized.

* Move to hg/git
  * git has several features that could be useful. In particular it allows for greater freedom in the development work flow. Ref: http://xkcd.com/1597/
  * Some developers expressed their concern about git forcing developers to change their work flow or making branching too easy.
  * For the moment we will try to set up an hybrid system where developers can use either git or svn.

* Status of the Frozen Density Embedding (FDE) code
  * The FDE code needs to follow the Octopus coding conventions.
  * The FDE code should be less intrusive into other parts of Octopus and avoid so much duplicated code.
  * José Rui should be more present.

* Default mixing and "negative density after mixing" warning
  * The default mixing will be changed to mix the potential.
  * The problem with the mixed density should be handled by zeroing the negative values and renormalizing the density.

* Length units of coordinates and geometries in input and output files (XYZ, PDB, XSF, etc) 
  * The code should follow all existing conventions and specifications. David will try to document that on the wiki. (Everything I know on the subject is at {{< manual "Visualization" "Visualization" >}}).
  * In all other cases the default units will be set to Angstroms.
  * The only way to override the behaviour should be by setting explicitly some option in the input file.

* Domain name
  * octopus-code.org had the preference of the majority.
  * Xavier will take care of registering the new domain.

* Another code named Octopus
  * Alberto will write an email to the other code developers to tell them about the name clash and to ask them if they are willing to change the name of their code.

* Status of the test suite and test farm
  * There are several tests failing for some time now.
  * A new ticket should be created whenever a test starts failing and is not fixed shortly afterwards.
  * A reminder (weekly?) of open tickets should be sent to the mailing list. David will investigate a similar scheme used by the buildbot developers: https://raw.githubusercontent.com/buildbot/buildbot-infra/master/files/weekly-summary.py

* Fortran 2003 object-oriented features
  * Old compilers are the major problem.
  * Old compilers in the test farm will be retired if they start hindering the adoption of new features broadly supported by newer compilers. 

* Social media presence
  * Developers are encouraged to post anything relevant/interesting on the Octopus Facebook account.

* Next meeting
  * It was decided that a meeting should be held at least once every six months.
  * Next meeting will be in March 2016. Xavier will take care of the organization.
  * There's the possibility to organize a meeting in Benasque in September 2016.
